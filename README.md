ECS 160 Project Android Version Group Members: Christopher Chan, Xiao Lin Li, Kiet Quach,Angela(Haowen) Zhou

#### PLEASE READ (contains more than just styling)
Styling and Coding Guide here: https://github.com/UCDClassNitta/ECS160Android/blob/master/Styling%20&%20Coding%20Guide.md

#####Quick Style Guide
Spacing: 4 Spaces (Android Studio default)  
Commenting: Single star lines (Android Studio default)  
PascalCase for FUNCTIONS and CLASSES  
camelCase for VARIABLES  

#### Project Structure
Names should be in PascalCase  
All ACTIVITIES belong in com.mygdx.game.android  
-names of activities should have "Activity" appended to the end of the activity name (i.e: SplashActivity)  
All HELPER CLASSES belong in core/helperclasses package  
-names of helper classes should have "Helper" appended to the end of the helper name (i.e: TileLoaderHelper)  
  
If implementing new features, PLEASE MAKE A NEW BRANCH  
-if you think the branch is ready for merging, please let members know on Flowdock so we can give a code review (for potential syntactical, symantical, code styling errors).

