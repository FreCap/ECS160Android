# Styling & Coding Guide

## _Styling_
### Class, ButtonID & Function Casing: PascalCase
ex:  
&nbsp;&nbsp;MyClassName  
&nbsp;&nbsp;IdClassName  

### Variable Casing: camelCase
ex:  

&nbsp;&nbsp;Correct-  
&nbsp;&nbsp;thisIsProperCasing  
&nbsp;&nbsp;idHere  
  
&nbsp;&nbsp;Incorrect-  
&nbsp;&nbsp;IDHere  
&nbsp;&nbsp;ProperNounShouldStillBeLowerCasedIfFirstWord  

### Code Layout: Stroustrup style  
ex:  
&nbsp;&nbsp;Correct-  

  
void foo(bool yeah)  
{  
&nbsp;&nbsp;if(yeah){  
&nbsp;&nbsp;&nbsp;&nbsp;System.println("yeah");  
&nbsp;&nbsp;}  
&nbsp;&nbsp;else{  
&nbsp;&nbsp;&nbsp;&nbsp;System.println("not yeah");  
&nbsp;&nbsp;}  
}  
  
&nbsp;&nbsp;Incorrect-  
void foo(bool nope){ //this needs to be on next line   
&nbsp;&nbsp;if(nope)  
&nbsp;&nbsp;{  
&nbsp;&nbsp;&nbsp;&nbsp;System.println("{ should have been after the parens at the if-statement");   
&nbsp;&nbsp;}  
&nbsp;&nbsp;else  
&nbsp;&nbsp;&nbsp;&nbsp;System.println("Missing {} even though this is a one line statement");  
}  

##Project Structure
Names should be in PascalCase  
All ACTIVITIES belong in com.mygdx.game.android  
&nbsp;&nbsp;-names of activities should have "Activity" appended to the end of the activity name (i.e: SplashActivity)  
All HELPER CLASSES belong in core/helperclasses package  
&nbsp;&nbsp;-names of helper classes should have "Helper" appended to the end of the helper name (i.e: TileLoaderHelper)  

## _General Coding Stuff_  
-Please give a meaningful ID to all buttons; set the ID to the name of the class you're going to be accessing (PASCAL CASE) with "Button" appended to the end of that ID. 
&nbsp;&nbsp;-Likewise, please give a meaningful name to all classes and variables and try not to shorten the names.  

