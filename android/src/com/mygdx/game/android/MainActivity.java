package com.mygdx.game.android;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
    private MediaPlayer mediaPlayer;

    public void playGame(View view) {
        //release and nullify mediaPlayer before game starts.
        mediaPlayer.release();
        mediaPlayer = null;

        Intent intent = new Intent(this, PlayGame.class);
        startActivity(intent);
    }


    public void playMultiplayer(View view) {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Pause music so it can start from where it left over when it resumes.
        //for situations like switching to another app or pressing home screen button.
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.menu);
            mediaPlayer.setLooping(true);
        }
        mediaPlayer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
    }
}
