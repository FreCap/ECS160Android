package multiplayer;

import java.util.List;

/**
 * Created by fre on 11/2/15.
 * <p>
 * ** Documentation **
 * https://github.com/UCDClassNitta/ECS160MultiPlayer/blob/master/documentation/ClientInterface.md
 */
public interface IGame {
    int createGame();

    int startGame();

    void sendAction(GameCommand asd);

    // in game function
    int getNextId(int color);

    //game
    boolean canTick();

    List<GameCommand> getActions();

    // callbacks function
    int gameTerminated();
}
