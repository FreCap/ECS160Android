package multiplayer;


import java.util.List;

import multiplayer.implementation.connection.view.GameItem;

/**
 * Created by fre on 11/2/15.
 */
public interface ILobby {
    int authenticate(String user, String pass);

    List<GameItem> getGameList();

    GameItem joinGame(int gameId);
}
