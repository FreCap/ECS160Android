package multiplayer;

import com.mygdx.game.GameDataTypes;

/**
 * Created by fre on 11/4/15.
 */
public class GameCommand {
    GameDataTypes.EAssetCapabilityType action;
    int actors[]; // units' id
    GameDataTypes.EPlayerColor TargetColor;
    GameDataTypes.EAssetType TargetType;
    GameType.Position TargetLocation;
}
