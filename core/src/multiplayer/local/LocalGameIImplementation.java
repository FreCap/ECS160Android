package multiplayer.local;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import multiplayer.GameCommand;
import multiplayer.IGame;

/**
 * Created by fre on 11/4/15.
 */
public class LocalGameIImplementation implements IGame {
    ConcurrentLinkedQueue<GameCommand> commandsQueue;
    Integer unitsCounter = 1;


    @Override
    public int createGame() {
        return 0;
    }

    @Override
    public int startGame() {
        return 0;
    }

    @Override
    public void sendAction(GameCommand gameCommand) {
        commandsQueue.add(gameCommand);
    }

    @Override
    public int getNextId(int color) {
        // the difference of color doesn't matter in single player
        return unitsCounter++;
    }

    @Override
    public boolean canTick() {
        return true;
    }

    @Override
    public List<GameCommand> getActions() {
        List<GameCommand> gameCommands = new ArrayList<GameCommand>();
        gameCommands.add(commandsQueue.poll());
        return gameCommands;
    }

    @Override
    public int gameTerminated() {
        return 0;
    }


    // ################### STRUCTURES, GETTER & SETTER ###############################
    public static synchronized LocalGameIImplementation getInstance() {
        if (ref == null) {
            ref = new LocalGameIImplementation();
        }
        return ref;
    }

    public Object clone()
            throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    private static LocalGameIImplementation ref;

}
