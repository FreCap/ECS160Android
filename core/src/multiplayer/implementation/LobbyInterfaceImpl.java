package multiplayer.implementation;


import java.util.List;
import java.util.Observable;
import java.util.Observer;

import multiplayer.ILobby;
import multiplayer.implementation.connection.entity.Player;
import multiplayer.implementation.connection.messages.request.auth_hello;
import multiplayer.implementation.connection.messages.response.auth_response;
import multiplayer.implementation.connection.view.GameItem;

/**
 * Created by fre on 11/2/15.
 */
public class LobbyInterfaceImpl implements ILobby {

    public static synchronized LobbyInterfaceImpl getInstance() {
        if (ref == null) {
            ref = new LobbyInterfaceImpl();
        }
        return ref;
    }

    public Object clone()
            throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    private static LobbyInterfaceImpl ref;

    private LobbyInterfaceImpl() {
    }

    @Override
    public int authenticate(String user, String pass) {
        auth_response.observable.addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                Player player = (Player) data;
                //Log.d("LOGIN", "Success");
                auth_response.observable.deleteObserver(this);
            }
        });
        new auth_hello(user, pass).send();

        return 0;
    }

    @Override
    public List<GameItem> getGameList() {
        return null;
    }

    @Override
    public GameItem joinGame(int gameId) {
        return null;
    }
}
