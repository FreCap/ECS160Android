package multiplayer.implementation.connection;


import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import multiplayer.implementation.connection.entity.Player;
import multiplayer.implementation.connection.messages.response.AbstractResponseMessage;


@ChannelHandler.Sharable
public class WarcraftHandler extends SimpleChannelInboundHandler<AbstractResponseMessage> {


    private Player player;

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        player = new Player(ctx.channel());
      //  Log.d("CONNECTION", "connection established");
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, AbstractResponseMessage message) throws Exception {

        if (player == null) {
            throw new Exception("The user should exist");
        }

        message.handle(player);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }


}
