package multiplayer.implementation.connection;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.Field;

import multiplayer.implementation.connection.messages.response.error.error;

public class Serializer {
    private static final JsonParser parser = new JsonParser();
    private static final Gson GSON = new Gson();

    static final class NullFieldException extends Exception {
        public NullFieldException(final String message) {
            super(message);
        }
    }

    public static class SerializationException extends Exception {
        private boolean isEncoding;

        public SerializationException(final String message, boolean isEncoding) {
            super(message);
            this.isEncoding = isEncoding;
        }

        public String getEncodedMessage() {
            return GSON.toJson(new error("Error " + (isEncoding ? "encoding" : "decoding") + " message: " + this.getMessage())) + "\r\n";
        }
    }

    public static Object decode(String bytes) throws SerializationException {
        try {
            final JsonObject json = (JsonObject) parser.parse(bytes);
            final Class clazz = Class.forName("multiplayer.implementation.connection.messages.response." + json.get("type").getAsString());
            final Object obj = GSON.fromJson(json, clazz);
            checkObject(json, obj);

            return obj;
        } catch (final Throwable cause) {
            final String errorMessage;
            if (cause instanceof ClassNotFoundException) {
                errorMessage = cause.getMessage() + " is not a valid message type";
            } else {
                errorMessage = (cause instanceof NullPointerException ? "Object did not have a type" : cause.getMessage());
            }

            throw new SerializationException(errorMessage, false);
        }
    }

    public static String encode(Object obj) throws SerializationException {
        try {
            //TODO check validity of obj?
            final String bytes = GSON.toJson(obj);

            //final JsonObject json = (JsonObject) parser.parse(bytes);
            //checkObject(json, obj);

            return bytes + "\r\n";
        } catch (final Throwable cause) {
            throw new SerializationException(cause.getMessage(), true);
        }
    }

    private static void checkObject(final JsonObject json, final Object object) throws Exception {
        checkObject(json, object, "msg");
    }

    private static void checkObject(final JsonObject json, final Object object, final String fieldName) throws Exception {
        for (final Field field : object.getClass().getFields()) {
            final String name = field.getName();
            checkField(json.get(name), field.get(object), fieldName + "[\"" + name + "\"]");
        }
    }

    private static void checkArray(final JsonArray json, final Object[] object, final String fieldName) throws Exception {
        for (int i = 0; i < object.length; i++) {
            checkField(json.get(i), object[i], fieldName + "[" + i + "]");
        }
    }

    private static void checkField(final Object jsonField, final Object javaField, final String fieldName) throws Exception {
        if (jsonField == null || jsonField == JsonNull.INSTANCE) {
            throw new NullFieldException(fieldName + " is null");
        } else if (javaField instanceof Object[]) {
            checkArray((JsonArray) jsonField, (Object[]) javaField, fieldName);
        } else if (
                javaField instanceof Byte
                        || javaField instanceof Short
                        || javaField instanceof Integer
                        || javaField instanceof Long
                        || javaField instanceof Float
                        || javaField instanceof Double
                        || javaField instanceof Boolean
                        || javaField instanceof Character
                        || javaField instanceof byte[]
                        || javaField instanceof short[]
                        || javaField instanceof int[]
                        || javaField instanceof long[]
                        || javaField instanceof float[]
                        || javaField instanceof double[]
                        || javaField instanceof boolean[]
                        || javaField instanceof char[]
                        || javaField instanceof String
                        || javaField instanceof String[]
                ) {
            //throw new Exception("LOL WOW");
        } else {
            checkObject((JsonObject) jsonField, javaField, fieldName);
        }
    }
}
