package multiplayer.implementation.connection.entity;

import java.util.Collections;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

public class GameRoom {


    public enum GameRoomState {
        WATING, PLAYNG, ENDED
    }

    private final int roomId;
    private String name;
    private GameRoomState state;
    private Set<Player> players = Collections.newSetFromMap(new ConcurrentHashMap<Player, Boolean>());

    private String map;

    public GameRoom(String name, int roomId) {
        state = GameRoomState.WATING;
        this.name = name;
        this.roomId = roomId;
    }

    public boolean playerSubscribe(Player subscriber) {
        if (state != GameRoomState.WATING) {
            return false;
        }
        players.add(subscriber);
        return true;
    }

    public boolean playerUnsubscribe(Player subscriber) {
        if (!(players.contains(subscriber)))
            return false;

        players.remove(subscriber);

        return true;
    }

    // ####################### GETTER AND SETTER SECTION #########################à

    public Set<Player> getPlayers() {
        return players;
    }

    public int getGameRoomId() {
        return roomId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GameRoomState getState() {
        return state;
    }

    public void setState(GameRoomState state) {
        this.state = state;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }
}
