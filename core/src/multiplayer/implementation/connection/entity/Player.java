package multiplayer.implementation.connection.entity;


import io.netty.channel.Channel;

public class Player {


    public enum PlayerState {
        ANONYMOUS, LOGGED, INGAME, DISCONNECTED
    }

    public enum PlayerRace {
        HUMAN, ORC
    }

    private Channel channel;
    private String name;
    private PlayerRace race;
    private PlayerState state;

    private GameRoom gameRoom;

    public Player(Channel c) {
        channel = c;
        state = PlayerState.DISCONNECTED;
    }

    public synchronized boolean login(String name) {
        if (getState() != PlayerState.ANONYMOUS) {
            return false;
        }

        // assume success

        setState(PlayerState.LOGGED);
        this.name = name;
        return true;
    }

    public boolean subscribe(GameRoom gameRoom) {
        setGameRoom(gameRoom);

        //default Human
        setRace(PlayerRace.HUMAN);

        boolean success = gameRoom.playerSubscribe(this);
        if (success == false) {
            setGameRoom(null);
            setRace(null);
            return false;
        } else {
            return true;
        }
    }

    public synchronized boolean unsubscribe() {
        if (!getGameRoom().playerUnsubscribe(this)) {
            return false;
        }

        setGameRoom(null);
        setRace(null);

        return true;
    }

    public boolean isAuthticated() {
        return state != PlayerState.ANONYMOUS;
    }

    // ####################### GETTER AND SETTER SECTION #########################à

    public GameRoom getGameRoom() {
        return gameRoom;
    }

    public boolean hasGameRoom() {
        return getGameRoom() != null;
    }

    public void setGameRoom(GameRoom gameRoom) {
        this.gameRoom = gameRoom;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getName() {
        return name;
    }

    public PlayerRace getRace() {
        return race;
    }

    public void setRace(PlayerRace race) {
        this.race = race;
    }

    public PlayerState getState() {
        return state;
    }

    public void setState(PlayerState state) {
        this.state = state;
    }

}