package multiplayer.implementation.connection;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Created by fre on 11/2/15.
 */
public class SocketManager {

    static final String HOST = "192.168.1.22";
    static final int PORT = 8007;

    Channel ch;

    public static synchronized SocketManager getInstance() {
        if (ref == null) {
            ref = new SocketManager();
        }
        return ref;
    }

    public Object clone()
            throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    private static SocketManager ref;

    private SocketManager() {

        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new WarcraftChannelInitializer(new WarcraftHandler()));

            // Start the connection attempt.
            ch = b.connect(HOST, PORT).sync().channel();

        } catch (Exception e) {
            System.out.println("#################################################################");
            System.out.println("################### FAILED OPENING CONNECTION ###################");
            System.out.println("#################################################################");
            e.printStackTrace();
            group.shutdownGracefully();


        } finally {
        }
    }

    public Channel getChannel() {
        return ch;
    }
}
