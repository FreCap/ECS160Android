package multiplayer.implementation.connection.messages.response;


import java.util.Observable;

import multiplayer.implementation.connection.entity.Player;

public class auth_response extends AbstractResponseMessage {

    public int lobby_id;
    public boolean success;

    static public Observable observable = new Observable();

    @Override
    public void handle(Player player) {
        if (success) {
            player.setState(Player.PlayerState.LOGGED);
        }
        observable.notifyObservers(player);
    }
}
