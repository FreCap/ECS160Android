package multiplayer.implementation.connection.messages.response;


import java.util.Collection;

import multiplayer.implementation.connection.view.PlayerItem;

public class lobby_gamePlayerList extends AbstractResponseMessage {

    public int game_id;
    public Collection<PlayerItem> players;


}
