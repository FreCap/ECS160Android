package multiplayer.implementation.connection.messages.request;

public class lobby_hostGame extends AbstractRequestMessage {

    public final String game_name;
    public final String map;

    public lobby_hostGame(final String game_name, final String map) {
        this.game_name = game_name;
        this.map = map;
    }

}
