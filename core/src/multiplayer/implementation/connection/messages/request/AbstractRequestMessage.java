package multiplayer.implementation.connection.messages.request;


import multiplayer.implementation.connection.SocketManager;
import multiplayer.implementation.connection.messages.BaseMessage;

public abstract class AbstractRequestMessage extends BaseMessage {


    public void send() {
        SocketManager.getInstance().getChannel().writeAndFlush(this);
    }

}
