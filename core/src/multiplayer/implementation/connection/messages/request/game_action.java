package multiplayer.implementation.connection.messages.request;

import com.google.gson.JsonObject;

public class game_action extends AbstractRequestMessage {

    public final JsonObject data;

    public game_action(final JsonObject data) {
        this.data = data;
    }
}
