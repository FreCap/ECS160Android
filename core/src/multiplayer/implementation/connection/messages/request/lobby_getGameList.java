package multiplayer.implementation.connection.messages.request;

public class lobby_getGameList extends AbstractRequestMessage {

    public int lobby_id;

    public lobby_getGameList(final int lobby_id) {
        this.lobby_id = lobby_id;
    }

}