package multiplayer.implementation.connection.messages.request;

public class auth_hello extends AbstractRequestMessage {

    public String user;
    public String pass;

    public auth_hello(final String user, final String pass) {
        this.user = user;
        this.pass = pass;
    }


}
