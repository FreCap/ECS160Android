package multiplayer.implementation.connection.messages.request;

public class lobby_joinGame extends AbstractRequestMessage {

    public final int game_id;

    public lobby_joinGame(final int game_id) {
        this.game_id = game_id;
    }

}
