package multiplayer.implementation;


import java.util.List;

import multiplayer.GameCommand;
import multiplayer.IGame;

/**
 * Created by fre on 11/2/15.
 */
public class GameInterfaceImpl implements IGame {

    public static synchronized GameInterfaceImpl getInstance() {
        if (ref == null) {
            ref = new GameInterfaceImpl();
        }
        return ref;
    }

    public Object clone()
            throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    private static GameInterfaceImpl ref;

    private GameInterfaceImpl() {
    }

    @Override
    public int createGame() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int startGame() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sendAction(GameCommand asd) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getNextId(int color) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean canTick() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<GameCommand> getActions() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int gameTerminated() {
        return 0;
    }
}
