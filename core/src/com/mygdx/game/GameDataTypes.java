package com.mygdx.game;

/**
 * Created by liray on 11/7/2015.
 */
public class GameDataTypes {
    public enum EPlayerColor {
        pcNone(0),
        pcBlue(1),
        pcRed(2),
        pcGreen(3),
        pcPurple(4),
        pcOrange(5),
        pcYellow(6),
        pcBlack(7),
        pcWhite(8),
        pcMax(9);
        private final int value;

        EPlayerColor(int value) {
            this.value = value;
        }
    }

    public enum EAssetAction {
        aaNone,
        aaConstruct,
        aaBuild,
        aaRepair,
        aaWalk,
        aaStandGround,
        aaAttack,
        aaHarvestLumber,
        aaMineGold,
        aaConveyLumber,
        aaConveyGold,
        // stone
        aaQuarryStone,
        aaConveyStone,
        aaDeath,
        aaDecay,
        aaCapability
    }

    public enum EAssetCapabilityType {
        actNone(0),
        actBuildPeasant(1),
        actBuildFootman(2),
        actBuildArcher(3),
        actBuildRanger(4),
        actBuildFarm(5),
        actBuildTownHall(6),
        actBuildBarracks(7),
        actBuildLumberMill(8),
        actBuildBlacksmith(9),
        actBuildKeep(10),
        actBuildCastle(11),
        actBuildScoutTower(12),
        actBuildGuardTower(13),
        actBuildCannonTower(14),
        actMove(15),
        actRepair(16),
        actMine(17),
        actBuildSimple(18),
        actBuildAdvanced(19),
        actConvey(20),
        actCancel(21),
        actBuildWall(22),
        actAttack(23),
        actStandGround(24),
        actPatrol(25),
        actWeaponUpgrade1(26),
        actWeaponUpgrade2(27),
        actWeaponUpgrade3(28),
        actArrowUpgrade1(29),
        actArrowUpgrade2(30),
        actArrowUpgrade3(31),
        actArmorUpgrade1(32),
        actArmorUpgrade2(33),
        actArmorUpgrade3(34),
        actLongbow(35),
        actRangerScouting(36),
        actMarksmanship(37),
        actMax(38);

        private final int value;

        EAssetCapabilityType(int value) {
            this.value = value;
        }
    }

    public enum EAssetType {
        atNone(0),
        atPeasant(1),
        atFootman(2),
        atArcher(3),
        atRanger(4),
        atGoldMine(5),
        atTownHall(6),
        atKeep(7),
        atCastle(8),
        atFarm(9),
        atBarracks(10),
        atLumberMill(11),
        atBlacksmith(12),
        atScoutTower(13),
        atGuardTower(14),
        atCannonTower(15),
        atMax(16);

        private final int value;

        EAssetType(int value) {
            this.value = value;
        }
    }

    public enum EDirection {
        dNorth,
        dNorthEast,
        dEast,
        dSouthEast,
        dSouth,
        dSouthWest,
        dWest,
        dNorthWest,
        dMax
    }
}
