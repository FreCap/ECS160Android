package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.ArrayList;
import java.util.HashMap;

import AssetActor.Archer;
import AssetActor.AssetType;
import AssetActor.Barracks;
import AssetActor.BlackSmith;
import AssetActor.Capability;
import AssetActor.Farm;
import AssetActor.Footman;
import AssetActor.GoldMine;
import AssetActor.LumberMill;
import AssetActor.Peasant;
import AssetActor.PlayerAsset;
import AssetActor.ScoutTower;
import AssetActor.Ranger;
import AssetActor.TownHall;
import InputListener.MapCameraGesturesListener;
import helperclasses.AssetRendererHelper;
import helperclasses.BuildingRendererHelper;
import helperclasses.SoundEffectHelper;
import helperclasses.TileLoaderHelper;
import helperclasses.UIHelper;


public class Play implements Screen {
    private Stage mainMapStage;

    //private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private OrthographicCamera camera;
    private TileLoaderHelper tileLoader;
    private Music music;
    private ShapeRenderer borderRenderer;
    private ShapeRenderer boxRenderer;
    private AssetRendererHelper assetRenderer;
    private UIHelper uiLoader;
    private OrthogonalTiledMapRenderer MinimapRenderer;
    protected OrthographicCamera MinimapCamera;
    private BuildingRendererHelper BuildingRenderer;
    private SpriteBatch batch;
    private Sprite backgroundTexture;
    private Sprite resourceSpriteGold;
    private Sprite resourceSpriteLumber;
    private Sprite resourceSpriteFood;
    private BitmapFont fontGold, fontLumber, fontFood;
    private static final int WIDTH = Gdx.graphics.getWidth();
    private static final int HEIGHT = Gdx.graphics.getHeight();
    Rectangle rect;
    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private InputMultiplexer inputMultiplexer;

    @Override
    public void show() {
        Global.nameAssetTypeRegistry = new HashMap<String, AssetType>();
        Capability.initializeNameTypeMap();
        AssetType.initializeNameToTypeMap();
        AssetType.loadTypes();
        Global.soundEffectHelper = new SoundEffectHelper();
        Global.CapabilitiesList = new HashMap<String, ArrayList<String>>();
        Capability.initializeCapabilityList();
        //setup and play ingame music
        music = Gdx.audio.newMusic(Gdx.files.internal("snd/music/game1.mid"));
        music.setLooping(true);

        music.play();
        music.setVolume(0.75f);
        // set up texture background
        batch = new SpriteBatch();
        backgroundTexture = new Sprite(new Texture(Gdx.files.internal("texture.png")));
        backgroundTexture.setSize(WIDTH, HEIGHT);


        Global.playerData = new PlayerData();

        //free type font generator
        generator = new FreeTypeFontGenerator(Gdx.files.internal("font/AliquamREG.ttf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = (int) (12 * Gdx.graphics.getDensity());
        System.out.println("fontsize=" + parameter.size);
        fontGold = generator.generateFont(parameter);
        fontLumber = generator.generateFont(parameter);
        fontFood = generator.generateFont(parameter);


        uiLoader = new UIHelper();
        MainGameViewport mainViewPort = new MainGameViewport(uiLoader.MapXposition, uiLoader.MapYPosition, uiLoader.MapStageWidth, uiLoader.MapStageHeight);
        mainMapStage = new Stage(mainViewPort);
        mainMapStage.getRoot().setSize(32 * 98, 32 * 66);
        mainMapStage.addListener(new MapCameraGesturesListener((OrthographicCamera) mainMapStage.getCamera()));

        tileLoader = new TileLoaderHelper();
        Global.map = tileLoader.RenderTiledMap2();
        tileLoader.RenderTiledMap();
        mainMapStage.addActor(Global.mapGroup);
        //mainMapStage.addActor(tileLoader.getTreeTopGroup());


        renderer = new OrthogonalTiledMapRenderer(Global.map);
        MinimapRenderer = new OrthogonalTiledMapRenderer(Global.map, 1 / 4f);

        MinimapCamera = new OrthographicCamera();
        MinimapCamera.setToOrtho(true, 800, 480);

        camera = new OrthographicCamera();
        camera.setToOrtho(true, 800, 480);
        camera.position.set(800, 480, 0); // Start in middle(I think) of map
        camera.update();

        //set up resource icons
        resourceSpriteGold = new Sprite(Global.terrainTiles.get("gold"));
        resourceSpriteGold.setSize(uiLoader.ResourceWidth, uiLoader.ResourceHeight);
        resourceSpriteGold.setPosition(uiLoader.GoldX, uiLoader.ResourceY);
        resourceSpriteGold.setRotation(180);
        //fontGold = new BitmapFont();
        fontGold.setColor(Color.WHITE);

        //resourceBatchLumber = new SpriteBatch();
        resourceSpriteLumber = new Sprite(Global.terrainTiles.get("lumber"));
        resourceSpriteLumber.setSize(uiLoader.ResourceWidth, uiLoader.ResourceHeight);
        resourceSpriteLumber.setPosition(uiLoader.LumberX, uiLoader.ResourceY);
        resourceSpriteLumber.setRotation(180);
        //fontLumber = new BitmapFont();
        fontLumber.setColor(Color.WHITE);

        //resourceBatchFood = new SpriteBatch();
        resourceSpriteFood = new Sprite(Global.terrainTiles.get("food"));
        resourceSpriteFood.setSize(uiLoader.ResourceWidth, uiLoader.ResourceHeight);
        resourceSpriteFood.setPosition(uiLoader.FoodX, uiLoader.ResourceY);
        resourceSpriteFood.setRotation(180);
        //fontFood = new BitmapFont();
        fontFood.setColor(Color.WHITE);

        assetRenderer = new AssetRendererHelper();
        BuildingRenderer = new BuildingRendererHelper();
        borderRenderer = new ShapeRenderer();
        boxRenderer = new ShapeRenderer();
        Global.selectedAssets = new ArrayList<PlayerAsset>();
        Global.playerAssets = new ArrayList<PlayerAsset>();

        Global.allTownHalls = new ArrayList<TownHall>();
        TownHall townHall = new TownHall(9, 9);
        // TownHall townHall2 = new TownHall(15, 40); FIND THE RIGHT LOCATION
        Group townHalls = new Group();
        townHalls.addActor(townHall);
        //townHalls.addActor(townHall2);
        mainMapStage.addActor(townHalls);


        //testing buildings

        Farm farm = new Farm(15, 9);
        mainMapStage.addActor(farm);
        Barracks barracks = new Barracks(20, 9);
        mainMapStage.addActor(barracks);
        LumberMill lumberm = new LumberMill(25, 9);
        mainMapStage.addActor(lumberm);
        BlackSmith willSmith = new BlackSmith(20, 15);
        mainMapStage.addActor(willSmith);
        ScoutTower scouts = new ScoutTower(15, 15);
        mainMapStage.addActor(scouts);

        Global.playerAssets.add(townHall);
        Global.playerAssets.add(farm);
        Global.playerAssets.add(lumberm);
        Global.playerAssets.add(willSmith);
        Global.playerAssets.add(scouts);
        Global.playerAssets.add(barracks);

        //testend

        Peasant peasant = new Peasant(5, 5);
        //Peasant peasant2 = new Peasant(7, 7);
        Archer archer = new Archer(6,16);
        Ranger ranger = new Ranger(6,20);
        Footman footman = new Footman(6,23);
        Global.archerGroup = new Group();
        Global.peasantGroup = new Group();
        Global.rangerGroup = new Group();
        Global.footmanGroup = new Group();
        Global.archerGroup.addActor(archer);
        Global.footmanGroup.addActor(footman);
        Global.rangerGroup.addActor(ranger);
        Global.peasantGroup.addActor(peasant);
        //Global.peasantGroup.addActor(peasant2);
        mainMapStage.addActor(Global.peasantGroup);
        mainMapStage.addActor(Global.archerGroup);
        mainMapStage.addActor(Global.footmanGroup);
        mainMapStage.addActor(Global.rangerGroup);

        // Global.playerAssets.add(spawnpeasant);
        Global.playerAssets.add(peasant);
        //Global.playerAssets.add(peasant2);
        Global.playerAssets.add(archer);
        Global.playerAssets.add(ranger);
        Global.playerAssets.add(footman);

        //goldMines = new ArrayList<GoldMine>();
        //goldMines.add(new GoldMine(1, 12));
        //goldMines.add(new GoldMine(92, 49));
        Global.selectedGoldMine = new ArrayList<GoldMine>();
        Global.allGoldMines = new ArrayList<GoldMine>();
        Group goldMinez = new Group();
        goldMinez.addActor(new GoldMine(1, 12));
        goldMinez.addActor(new GoldMine(92, 49));
        //goldMinez.setDebug(true, true);
        mainMapStage.addActor(goldMinez);

        rect = new Rectangle();

        inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(0, uiLoader.uiStage);
        inputMultiplexer.addProcessor(1, mainMapStage);
        Gdx.input.setInputProcessor(inputMultiplexer);
        
    }


    private void drawSelectedAssetBorder() { //fine for stationary objects, moving ones need to be able to affect the rendering with animation
        borderRenderer.setProjectionMatrix(mainMapStage.getCamera().combined);
        borderRenderer.begin(ShapeRenderer.ShapeType.Line);

        float x2, y2;

        for (PlayerAsset playerAsset : Global.selectedAssets) {
            if (!playerAsset.isVisible()) {
                continue;
            }
            switch (playerAsset.getType()) {
                case atPeasant:
                    rect.set(0, 0, 32, 32);
                    float x1 = playerAsset.getTileX() * 32 + ((Peasant) playerAsset).getOffsetX();
                    float y1 = playerAsset.getTileY() * 32 + ((Peasant) playerAsset).getOffsetY();
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x1, y1, rect.width, rect.height);
                    break;
                case atArcher:
                    rect.set(0, 0, 32, 32);
                    float x3 = playerAsset.getTileX() * 32 + ((Archer) playerAsset).getOffsetX();
                    float y3 = playerAsset.getTileY() * 32 + ((Archer) playerAsset).getOffsetY();
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x3, y3, rect.width, rect.height);
                    break;
                case atRanger:
                    rect.set(0, 0, 32, 32);
                    float x4 = playerAsset.getTileX() * 32 + ((Ranger) playerAsset).getOffsetX();
                    float y4 = playerAsset.getTileY() * 32 + ((Ranger) playerAsset).getOffsetY();
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x4, y4, rect.width, rect.height);
                    break;
                case atFootman:
                    rect.set(0, 0, 32, 32);
                    float x5 = playerAsset.getTileX() * 32 + ((Footman) playerAsset).getOffsetX();
                    float y5 = playerAsset.getTileY() * 32 + ((Footman) playerAsset).getOffsetY();
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x5, y5, rect.width, rect.height);
                    break;
                case atTownHall:
                    rect.set(0, 0, 128, 128);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
                case atFarm:
                    rect.set(0, 0, 64, 64);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
                case atBarracks:
                    rect.set(0, 0, 96, 96);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
                case atBlacksmith:
                    rect.set(0, 0, 96, 96);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
                case atLumberMill:
                    rect.set(0, 0, 96, 96);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
                case atScoutTower:
                    rect.set(0, 0, 64, 64);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
                case atKeep:
                    rect.set(0, 0, 128, 128);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
                case atCastle:
                    rect.set(0, 0, 128, 128);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
                case atCannonTower:
                    rect.set(0, 0, 64, 64);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
                case atGuardTower:
                    rect.set(0, 0, 64, 64);
                    x2 = playerAsset.getTileX() * 32;
                    y2 = playerAsset.getTileY() * 32;
                    borderRenderer.setColor(new Color(0, 1, 0, 1));
                    borderRenderer.rect(x2, y2, rect.width, rect.height);
                    break;
            }
        }
        borderRenderer.end();
    }

    private void drawMinimapAssets() {
        boxRenderer.setProjectionMatrix(MinimapCamera.combined);
        boxRenderer.begin(ShapeRenderer.ShapeType.Filled);
        for (PlayerAsset playerAsset : Global.playerAssets) {
            if (!playerAsset.isVisible()) {
                continue;
            }
            rect.set(0, 0, 4, 4); //much less
            float x1 = playerAsset.getTileX() * 8;
            float y1 = playerAsset.getTileY() * 8;
            boxRenderer.setColor(new Color(0, 1, 0, 1));
            boxRenderer.rect(x1, y1, rect.width, rect.height);
        }

        for (PlayerAsset buildingAsset : Global.allTownHalls) {
            rect.set(0, 0, 16, 16);
            float x2 = buildingAsset.getTileX() * 8;
            float y2 = buildingAsset.getTileY() * 8;
            boxRenderer.setColor(new Color(0, 1, 0, 1));
            boxRenderer.rect(x2, y2, rect.width, rect.height);
        }

        for (PlayerAsset goldMine : Global.allGoldMines) { //should be all goldmines
            rect.set(0, 0, 8, 8);
            float x2 = goldMine.getTileX() * 8;
            float y2 = goldMine.getTileY() * 8;
            boxRenderer.setColor(new Color(Color.GOLD));
            boxRenderer.rect(x2, y2, rect.width, rect.height);
        }
        boxRenderer.end();
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        //set viewport for
        Gdx.gl.glViewport(0, 0, WIDTH, HEIGHT);

        // draw background
        batch.begin();
        backgroundTexture.draw(batch);

        //draw resource
        //gold
        resourceSpriteGold.setPosition(WIDTH * 900 / 2560, HEIGHT * 1420 / 1440);
        resourceSpriteGold.draw(batch);
        fontGold.draw(batch, Integer.toString(Global.playerData.getGold()), WIDTH * 920 / 2560, HEIGHT * 1420 / 1440);
        //lumber
        resourceSpriteLumber.setPosition(WIDTH * 1500 / 2560, HEIGHT * 1420 / 1440);
        resourceSpriteLumber.draw(batch);
        fontLumber.draw(batch, Integer.toString(Global.playerData.getLumber()), WIDTH * 1520 / 2560, HEIGHT * 1420/1440);
        //food
        resourceSpriteFood.setPosition(WIDTH * 2100 / 2560, HEIGHT * 1420 / 1440);
        resourceSpriteFood.draw(batch);
        fontFood.draw(batch, "3/5", WIDTH * 2120 / 2560, HEIGHT * 1420 / 1440);
        batch.end();

        //draw UI stage
        Gdx.gl.glViewport(uiLoader.UIXposition, uiLoader.UIYPosition, uiLoader.UIStageWidth, uiLoader.UIStageHeight);
        uiLoader.uiStage.act(delta);
        uiLoader.uiStage.draw();

        //draw Minimap
        Gdx.gl.glViewport(uiLoader.MinimapX, uiLoader.MinimapY, uiLoader.MinimapWidth + WIDTH * 55 / 512, uiLoader.MinimapHeight);
        MinimapRenderer.setView(MinimapCamera);
        MinimapRenderer.render();
        drawMinimapAssets();

        //Draw full map
        Gdx.gl.glViewport(uiLoader.MapXposition, uiLoader.MapYPosition, uiLoader.MapStageWidth, uiLoader.MapStageHeight);
        renderer.setView((OrthographicCamera) mainMapStage.getCamera());
        renderer.render();

        mainMapStage.act(delta);
        mainMapStage.getViewport().apply();
        mainMapStage.draw();
        //BuildingRenderer.renderBuilding(BuildingAssets, mainMapStage.getCamera());
        drawSelectedAssetBorder();

/*        if(!Global.selectedGoldMine.isEmpty()){
            for(int i=0; i<Global.playerAssets.size(); i++){
                if(Global.playerAssets.get(i).getBounds().overlaps(Global.selectedGoldMine.get(0).getBounds())){
                    System.out.println("They are touching");
                    Global.playerAssets.get(i).startMining();
                }
            }

        }

        for(int i=0; i<Global.playerAssets.size(); i++){
            if(Global.playerAssets.get(i).getBounds().overlaps(Global.allTownHalls.get(0).getBounds())){
                System.out.println("They are touching");
                Global.playerAssets.get(i).backToMining();
            }
        }*/
    }

    @Override
    public void resize(int width, int height) {
        ;
        mainMapStage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        Global.map.dispose();
        renderer.dispose();
        music.dispose();
        generator.dispose();
    }

    public OrthographicCamera getCamera() {
        return (OrthographicCamera) mainMapStage.getCamera();
    }


}
