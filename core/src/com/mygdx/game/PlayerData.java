package com.mygdx.game;

public class PlayerData {
    private int gold;
    private int lumber;
    private int stone;

    public PlayerData() {
        gold = 1000;
        lumber = 1000;
        stone = 1000;
    }

    public void changeGoldAmount(int gold) {
        this.gold += gold;
    }

    public void changeLumberAmount(int lumber) {
        this.lumber += lumber;
    }

    public void changeStonesAmount(int stone){
        this.stone += stone;
    }

    public int getStone() {
        return stone;
    }

    public int getGold() {
        return gold;
    }

    public int getLumber() {
        return lumber;
    }
}
