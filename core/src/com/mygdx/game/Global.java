package com.mygdx.game;


import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;
import java.util.HashMap;

import AssetActor.AssetType;
import AssetActor.GoldMine;
import AssetActor.Peasant;
import AssetActor.PlayerAsset;
import AssetActor.TileActor;
import AssetActor.TownHall;
import helperclasses.SoundEffectHelper;
import helperclasses.TileLoaderHelper;
import oracle.jrockit.jfr.StringConstantPool;

public class Global {
    public static HashMap<String, AssetType> nameAssetTypeRegistry;

    public static TiledMap map;
    public static char[][] storedMapArray;


    public static ArrayList<PlayerAsset> playerAssets;
    public static ArrayList<PlayerAsset> selectedAssets;
    public static TileActor selectedTileActor;
    public static ArrayList<GoldMine> selectedGoldMine;
    public static ArrayList<TownHall> allTownHalls;
    public static ArrayList<GoldMine> allGoldMines;
    public static Group peasantGroup;
    public static Group archerGroup;
    public static Group rangerGroup;
    public static Group footmanGroup;
    public static Group arrowGroup;
    public static HashMap<String, TextureRegion> terrainTiles;
    public static Group mapGroup;
    public static HashMap<String, ArrayList<TextureRegion>> tileSet;

    public static ArrayList<ArrayList<TextureRegion>> buildingTiles;
    public static ArrayList<ArrayList<Animation>> peasantAnimations;
    public static ArrayList<ArrayList<TextureRegion>> peasantStatics;
    public static ArrayList<ArrayList<Animation>> archerAnimations;
    public static ArrayList<ArrayList<TextureRegion>> archerStatics;
    public static ArrayList<ArrayList<Animation>> rangerAnimations;
    public static ArrayList<ArrayList<TextureRegion>> rangerStatics;
    public static ArrayList<ArrayList<Animation>> footmanAnimations;
    public static ArrayList<Animation> arrowAnimations;
    public static ArrayList<ArrayList<TextureRegion>> footmanStatics;
    public static PlayerData playerData;
    public static SoundEffectHelper soundEffectHelper;
    public static HashMap<String, ArrayList<String>> CapabilitiesList;
}
