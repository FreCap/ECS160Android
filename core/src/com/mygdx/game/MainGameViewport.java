package com.mygdx.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by liray on 11/1/2015.
 */
public class MainGameViewport extends Viewport {

    public MainGameViewport(int x, int y, int width, int height) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(true);
        setCamera(camera);
        setScreenBounds(x, y, width, height);
        setWorldSize(800, 480);
        //getCamera().translate(64, 0, 0);
        //getCamera().update();
    }
}
