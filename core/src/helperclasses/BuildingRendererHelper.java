package helperclasses;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.Global;

import java.util.ArrayList;

/**
 * Created by DrMario on 10/31/2015.
 */
public class BuildingRendererHelper {
    final String[] STATE = {"construct-0", "construct-1", "inactive", "place"};

    public BuildingRendererHelper() {
        Global.buildingTiles = new ArrayList<ArrayList<TextureRegion>>();
        for (String bState : STATE) {
            bState = "town" + bState;
            ArrayList<TextureRegion> currDirTiles = new ArrayList<TextureRegion>();
            TextureRegion tile = Global.terrainTiles.get(bState);
            currDirTiles.add(tile);
            Global.buildingTiles.add(currDirTiles);
        }
    }
}
