package helperclasses;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

/**
 * Created by Chris on 11/1/2015.
 * Contains button/unit functionality
 * ButtonHelper will pass its ID into the function helper and will call the appropriate function
 * The ID of the button has to correspond to the Function's switch "ID"
 */
public class ButtonFunctionHelper{

    public void ButtonFunction(int id, int unitID){
        switch(id){
            case 1:
                ChopTree(unitID);
                break;
            case 2: break;
            case 3: break;
            case 4: break;
            case 5: break;
            case 6: break;
            case 7: break;
            case 8: break;
            case 9: break;
            case 0: break;
        } //the cases are to be replaced with enum values
    }

    public static void ChopTree(int peasantID){

    }
}
