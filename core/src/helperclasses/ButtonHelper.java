package helperclasses;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

/**
 * Created by Chris on 11/1/2015.
 */
public class ButtonHelper extends Button {
    public int id;
    Sprite buttonSprite;

    public ButtonHelper(int id){
        this.id = id;
    }

}
