package helperclasses;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.mygdx.game.Global;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import AssetActor.TileActor;

/**
 * Created by Kiet Quach on 10/18/2015.
 * This contains the logic for rendering the main game map
 */
public class TileLoaderHelper {

    private TextureRegion tile;
    private TiledMap map;
    private MapLayers layer;
    private TiledMapTileLayer tileLayer;
    public static TiledMapTileLayer treeTopLayer;
    private TiledMapTileLayer.Cell cell;
    //private Group treeTopGroup;

    protected Vector<Integer> hamming;

    public TileLoaderHelper() {
        Global.storedMapArray = new char[66][98];

        Global.terrainTiles = new HashMap<String, TextureRegion>();
        hamming = new Vector<Integer>();
        InitializeTileSet();
        StoreMapArray();
        setTreeArrayList();
        setArrayList("grass");
        AliasDirt();
        AliasRock();

        Global.mapGroup = new Group();
        //treeTopGroup = new Group();


        map = new TiledMap();
        layer = map.getLayers();
        tileLayer = new TiledMapTileLayer(98, 66, 32, 32);
        treeTopLayer = new TiledMapTileLayer(98, 66, 32, 32);
    }

    public void InitializeTileSet() {
        Global.tileSet = new HashMap<String, ArrayList<TextureRegion>>();

        ArrayList<TextureRegion> grassArrayList = new ArrayList<TextureRegion>();
        Global.tileSet.put("grass", grassArrayList);
        ArrayList<TextureRegion> dirtArrayList = new ArrayList<TextureRegion>();
        Global.tileSet.put("dirt", dirtArrayList);
        ArrayList<TextureRegion> rockArrayList = new ArrayList<TextureRegion>();
        Global.tileSet.put("rock", rockArrayList);
        ArrayList<TextureRegion> treeArrayList = new ArrayList<TextureRegion>();
        Global.tileSet.put("tree", treeArrayList);
        ArrayList<TextureRegion> stumpArrayList = new ArrayList<TextureRegion>();
        Global.tileSet.put("stump", stumpArrayList);
        ArrayList<TextureRegion> waterArrayList = new ArrayList<TextureRegion>();
        Global.tileSet.put("water", waterArrayList);
        ArrayList<TextureRegion> wallArrayList = new ArrayList<TextureRegion>();
        Global.tileSet.put("wall", wallArrayList);
        ArrayList<TextureRegion> wallDamagedArrayList = new ArrayList<TextureRegion>();
        Global.tileSet.put("wall-damaged", wallDamagedArrayList);
        ArrayList<TextureRegion> rubbleArrayList = new ArrayList<TextureRegion>();
        Global.tileSet.put("rubble", rubbleArrayList);
        //ArrayList<TextureRegion> assetsArrayList = new ArrayList<TextureRegion>();

        SplitAndLoadPng("Terrain.dat", "Terrain.png", "");
        SplitAndLoadPng("GoldMine.dat", "GoldMine.png", "gold");
        SplitAndLoadPng("Peasant.dat", "Peasant.png", "peasant");

        // Building Assets
        SplitAndLoadPng("TownHall.dat", "TownHall.png", "town-hall");
        SplitAndLoadPng("Farm.dat", "Farm.png", "chicken-farm");
        SplitAndLoadPng("Barracks.dat", "Barracks.png", "human-barracks");
        SplitAndLoadPng("Blacksmith.dat", "Blacksmith.png", "human-blacksmith");
        SplitAndLoadPng("LumberMill.dat", "LumberMill.png", "human-lumber-mill");
        SplitAndLoadPng("ScoutTower.dat", "ScoutTower.png", "scout-tower");
        //SplitAndLoadPng("Keep.dat", "Keep.png", "keep");
        //SplitAndLoadPng("Castle.dat", "Castle.png", "castle");
        //SplitAndLoadPng("CannonTower.dat", "CannonTower.png", "cannontower");
        //SplitAndLoadPng("GuardTower.dat", "GuardTower.png", "guardtower");

        SplitAndLoadPng("Archer.dat", "Archer.png", "archer");
        SplitAndLoadPng("Ranger.dat", "Ranger.png", "ranger");
        SplitAndLoadPng("Footman.dat", "Footman.png", "footman");
        SplitAndLoadPng("MiniIcons.dat","MiniIcons.png", "");
        SplitAndLoadPng("Arrow.dat", "Arrow.png", "arrow");

    }

    private void SplitAndLoadPng(String datFileName, String pngFileName, String prefix) {
        final int PART_HEIGHT = 4096;

        int numTiles;
        String tileName;

        FileHandle handle = Gdx.files.internal(datFileName);
        InputStream inStream = handle.read();
        try {
            if (inStream != null) {
                BufferedReader buffReader = new BufferedReader(new InputStreamReader(inStream));
                buffReader.readLine(); //useless first line.
                numTiles = Integer.parseInt(buffReader.readLine()); //number of tiles in PNG

                handle = Gdx.files.internal(pngFileName);
                Pixmap pixmap = new Pixmap(handle);
                int pngWidth = pixmap.getWidth(); //pngWidth is same as tileWidth
                int pngHeight = pixmap.getHeight();

                int tileHeight = pngHeight / numTiles;
                int numParts = pngHeight / PART_HEIGHT; //number of FULL (height==2048) png parts


                int numTilesInCurrentPart;
                for (int i = 0; i < numParts + 1; i++) {//for each PNG part.+1 is for the last part
                    numTilesInCurrentPart = PART_HEIGHT / tileHeight;

                    if (i == numParts) {
                        if (pngHeight % PART_HEIGHT == 0) {
                            break;
                        }
                        numTilesInCurrentPart = (pngHeight - (numParts * PART_HEIGHT)) / tileHeight;
                    }

                    Pixmap part = new Pixmap(pngWidth, PART_HEIGHT, Pixmap.Format.RGBA8888);
                    part.drawPixmap(pixmap, 0, 0, 0, i * PART_HEIGHT, pngWidth, PART_HEIGHT); //Draw region of whole terrain
                    Texture texture = new Texture(part);

                    for (int j = 0; j < numTilesInCurrentPart; j++) {
                        tileName = buffReader.readLine();
                        tile = new TextureRegion(texture, 0, j * tileHeight, pngWidth, tileHeight);
                        tile.flip(false, true);
                        Global.terrainTiles.put(prefix + tileName, tile);
                        //tileSet.get(GetTileSetType(tileName)).add(tile);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String GetTileSetType(String tileName){
        return tileName.substring(0, tileName.lastIndexOf("-")-1);
    }//grabs the tile set name of a given tile. Ex: wall-damaged-255 = wall-damaged

    public void RenderTiledMap(){

        for(int i = 1; i < Global.storedMapArray.length - 1; i++){
            for(int j = 1; j < Global.storedMapArray[i].length - 1; j++){
                cell = new TiledMapTileLayer.Cell();
                int index;
                switch (Global.storedMapArray[i][j]) {
                    case 'F':
                        index = getCorrectTile("F", i, j);
                        if(index != 255){
                            Global.mapGroup.addActor(new TileActor(Global.tileSet.get("tree").get(getCorrectTreeTile("F", i, j)), "F", i, j, false));
                        }
                        break;
                    case 'R':
                        index = getCorrectTile("R", i, j);
                        if(index != 255){
                            Global.mapGroup.addActor(new TileActor(Global.tileSet.get("rock").get(index), "R", i, j, false));
                        }
                        break;
                }
            }
        }

        layer.add(tileLayer);
        layer.add(treeTopLayer);

    }

    public TiledMap RenderTiledMap2(){

        for(int i = 1; i < Global.storedMapArray.length - 1; i++){
            for(int j = 1; j < Global.storedMapArray[i].length - 1; j++){
                cell = new TiledMapTileLayer.Cell();
                switch (Global.storedMapArray[i][j]) {
                    case 'G':
                        int grassResult = getCorrectTile("G", i, j);
                        if(grassResult == 255){
                            cell.setTile(new StaticTiledMapTile(Global.terrainTiles.get("grass-0")));
                        }
                        else{
                            cell.setTile(new StaticTiledMapTile(Global.tileSet.get("dirt").get(grassResult)));
                        }
                        tileLayer.setCell(j, i, cell);
                        CheckIfTreeTop(i, j);
                        break;
                    case 'F':
                        cell.setTile(new StaticTiledMapTile(Global.tileSet.get("tree").get(getCorrectTreeTile("F", i, j))));
                        tileLayer.setCell(j, i, cell);
                        break;
                    case 'R':
                        cell.setTile(new StaticTiledMapTile(Global.tileSet.get("rock").get(getCorrectTile("R", i, j))));
                        tileLayer.setCell(j, i, cell);
                        break;
                    case 'D':
                        //cell.setTile(new StaticTiledMapTile(Global.tileSet.get("dirt").get(getCorrectTile("D", i, j))));
                        cell.setTile(new StaticTiledMapTile(Global.tileSet.get("dirt").get(255)));
                        tileLayer.setCell(j, i, cell);
                        CheckIfTreeTop(i, j);
                        break;

                }
            }
        }


        layer.add(tileLayer);
        //layer.add(secondLayer);
        layer.add(treeTopLayer);

        return map;
    }



    public void StoreMapArray(){
        BufferedReader buffReader;
        int row = 0;
        String line;

        try {
            FileHandle handle = Gdx.files.internal("2player.map");
            InputStream inStream = handle.read();
            if (inStream != null) {
                InputStreamReader inStreamReader = new InputStreamReader(inStream);
                buffReader = new BufferedReader(inStreamReader);
                buffReader.readLine(); //Useless first line
                buffReader.readLine(); //Dimension of map

                while(row < 66){

                    line = buffReader.readLine();
                    for(int i = 0; i < line.length(); i++){
                        Global.storedMapArray[row][i] = line.charAt(i);
                    }

                    row++;

                }

            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setArrayList(String type){
        for(int i = 0; i < 256; i++){
            if(Global.terrainTiles.containsKey(type + "-" +Integer.toString(i))){
                Global.tileSet.get(type).add(Global.terrainTiles.get(type + "-" +Integer.toString(i)));
            }
            else{
                /*if(type == "rock"){
                    Global.tileSet.get(type).add(Global.terrainTiles.get("rock-0"));
                }
                else if(type == "dirt"){
                    Global.tileSet.get(type).add(Global.terrainTiles.get("dirt-2"));
                }*/
                Global.tileSet.get(type).add(Global.terrainTiles.get(type + "-ab"));
                //Global.tileSet.get(type).add(null);
            }
        }
    }

    //apparently tree tiles can be rendered on grass tiles even though the tiles is a "g". This is done to render tree tops.
    private void setTreeArrayList(){
        for(int i = 0; i < 64; i++){
            if(Global.terrainTiles.containsKey("tree-"+Integer.toString(i))){
                Global.tileSet.get("tree").add(Global.terrainTiles.get("tree-"+Integer.toString(i)));
            }
            else{
                Global.tileSet.get("tree").add(Global.terrainTiles.get("tree-0"));
            }
        }

        Global.tileSet.get("tree").set(0x0A, Global.tileSet.get("tree").get(0x03));
        Global.tileSet.get("tree").set(0x0B, Global.tileSet.get("tree").get(0x03));
        Global.tileSet.get("tree").set(0x23, Global.tileSet.get("tree").get(0x03));
        Global.tileSet.get("tree").set(0x2B, Global.tileSet.get("tree").get(0x03));
        Global.tileSet.get("tree").set(0x0E, Global.tileSet.get("tree").get(0x06));
        Global.tileSet.get("tree").set(0x22, Global.tileSet.get("tree").get(0x06));
        Global.tileSet.get("tree").set(0x26, Global.tileSet.get("tree").get(0x06));
        Global.tileSet.get("tree").set(0x2E, Global.tileSet.get("tree").get(0x06));
        Global.tileSet.get("tree").set(0x0F, Global.tileSet.get("tree").get(0x07));
        Global.tileSet.get("tree").set(0x27, Global.tileSet.get("tree").get(0x07));
        Global.tileSet.get("tree").set(0x2F, Global.tileSet.get("tree").get(0x07));
        Global.tileSet.get("tree").set(0x18, Global.tileSet.get("tree").get(0x11));
        Global.tileSet.get("tree").set(0x19, Global.tileSet.get("tree").get(0x11));
        Global.tileSet.get("tree").set(0x39, Global.tileSet.get("tree").get(0x11));
        Global.tileSet.get("tree").set(0x30, Global.tileSet.get("tree").get(0x14));
        Global.tileSet.get("tree").set(0x34, Global.tileSet.get("tree").get(0x14));
        Global.tileSet.get("tree").set(0x32, Global.tileSet.get("tree").get(0x16));
        Global.tileSet.get("tree").set(0x3A, Global.tileSet.get("tree").get(0x1A));

        Global.tileSet.get("tree").set(0x11, Global.tileSet.get("tree").get(0x10));
        Global.tileSet.get("tree").set(0x14, Global.tileSet.get("tree").get(0x10));
        Global.tileSet.get("tree").set(0x15, Global.tileSet.get("tree").get(0x10));

        Global.tileSet.get("tree").set(0x39, Global.tileSet.get("tree").get(0x38));
        Global.tileSet.get("tree").set(0x3C, Global.tileSet.get("tree").get(0x38));
        Global.tileSet.get("tree").set(0x3D, Global.tileSet.get("tree").get(0x11));

    }

    private void AliasRock(){
        setArrayList("rock");

        //HammingSet stuff here
        MakeHammingSet(0x25, hamming);
        for(int Value : hamming){
            if(Global.terrainTiles.get("rock-ab").equals(Global.tileSet.get("rock").get(Value | 0x0A))){
                //System.out.println("Setting Alias From Hamming");
                Global.tileSet.get("rock").set((Value | 0x0A), Global.tileSet.get("rock").get(0x0B));
            }
        }

        MakeHammingSet(0x85, hamming);
        for(int Value : hamming){
            if(Global.terrainTiles.get("rock-ab").equals(Global.tileSet.get("rock").get(Value | 0x12))){
                //System.out.println("Setting Alias From Hamming");
                Global.tileSet.get("rock").set((Value | 0x12), Global.tileSet.get("rock").get(0x16));
            }
        }

        MakeHammingSet(0xA1, hamming);
        for(int Value : hamming){
            if(Global.terrainTiles.get("rock-ab").equals(Global.tileSet.get("rock").get(Value | 0x48))){
                //System.out.println("Setting Alias From Hamming");
                Global.tileSet.get("rock").set((Value | 0x48), Global.tileSet.get("rock").get(0x68));
            }
        }

        MakeHammingSet(0xA4, hamming);
        for(int Value : hamming){
            if(Global.terrainTiles.get("rock-ab").equals(Global.tileSet.get("rock").get(Value | 0x50))){
                //System.out.println("Setting Alias From Hamming");
                Global.tileSet.get("rock").set((Value | 0x50), Global.tileSet.get("rock").get(0xD0));
            }
        }

        Global.tileSet.get("rock").set(0x1B, Global.tileSet.get("rock").get(0x1F));
        Global.tileSet.get("rock").set(0x1E, Global.tileSet.get("rock").get(0x1F));
        Global.tileSet.get("rock").set(0x3F, Global.tileSet.get("rock").get(0x1F));
        Global.tileSet.get("rock").set(0x9F, Global.tileSet.get("rock").get(0x1F));

        Global.tileSet.get("rock").set(0x6F, Global.tileSet.get("rock").get(0x6B));
        Global.tileSet.get("rock").set(0xEB, Global.tileSet.get("rock").get(0x6B));

        Global.tileSet.get("rock").set(0xD7, Global.tileSet.get("rock").get(0xD6));
        Global.tileSet.get("rock").set(0xF6, Global.tileSet.get("rock").get(0xD6));

        Global.tileSet.get("rock").set(0x78, Global.tileSet.get("rock").get(0xF8));
        Global.tileSet.get("rock").set(0xD8, Global.tileSet.get("rock").get(0xF8));
        Global.tileSet.get("rock").set(0xF9, Global.tileSet.get("rock").get(0xF8));
        Global.tileSet.get("rock").set(0xFC, Global.tileSet.get("rock").get(0xF8));

        Global.tileSet.get("rock").set(0x09, Global.tileSet.get("rock").get(0x08));
        Global.tileSet.get("rock").set(0x28, Global.tileSet.get("rock").get(0x08));
        Global.tileSet.get("rock").set(0x29, Global.tileSet.get("rock").get(0x08));

        Global.tileSet.get("rock").set(0x14, Global.tileSet.get("rock").get(0x10));
        Global.tileSet.get("rock").set(0x90, Global.tileSet.get("rock").get(0x10));
        Global.tileSet.get("rock").set(0x94, Global.tileSet.get("rock").get(0x10));

        MakeHammingSet(0xA5, hamming);
        for(int Value : hamming){
            if(Global.terrainTiles.get("rock-ab").equals(Global.tileSet.get("rock").get(Value))){
                //System.out.println(Global.tileSet.get("rock").get(Value));
                Global.tileSet.get("rock").set((Value), Global.tileSet.get("rock").get(0x02));
                //System.out.println(Global.tileSet.get("rock").get(Value));
            }
        }
    }

    private void AliasDirt(){
        setArrayList("dirt");

        // Three in a row on side
        Global.tileSet.get("dirt").set(0x03, Global.tileSet.get("dirt").get(0x02));
        Global.tileSet.get("dirt").set(0x05, Global.tileSet.get("dirt").get(0x02));
        Global.tileSet.get("dirt").set(0x06, Global.tileSet.get("dirt").get(0x02));
        Global.tileSet.get("dirt").set(0x07, Global.tileSet.get("dirt").get(0x02));

        Global.tileSet.get("dirt").set(0x09, Global.tileSet.get("dirt").get(0x08));
        Global.tileSet.get("dirt").set(0x21, Global.tileSet.get("dirt").get(0x08));
        Global.tileSet.get("dirt").set(0x28, Global.tileSet.get("dirt").get(0x08));
        Global.tileSet.get("dirt").set(0x29, Global.tileSet.get("dirt").get(0x08));

        Global.tileSet.get("dirt").set(0x14, Global.tileSet.get("dirt").get(0x10));
        Global.tileSet.get("dirt").set(0x84, Global.tileSet.get("dirt").get(0x10));
        Global.tileSet.get("dirt").set(0x90, Global.tileSet.get("dirt").get(0x10));
        Global.tileSet.get("dirt").set(0x94, Global.tileSet.get("dirt").get(0x10));

        Global.tileSet.get("dirt").set(0x60, Global.tileSet.get("dirt").get(0x40));
        Global.tileSet.get("dirt").set(0xA0, Global.tileSet.get("dirt").get(0x40));
        Global.tileSet.get("dirt").set(0xC0, Global.tileSet.get("dirt").get(0x40));
        Global.tileSet.get("dirt").set(0xE0, Global.tileSet.get("dirt").get(0x40));

        // Corner three
        Global.tileSet.get("dirt").set(0x0A, Global.tileSet.get("dirt").get(0x0B));
        Global.tileSet.get("dirt").set(0x0F, Global.tileSet.get("dirt").get(0x0B));
        Global.tileSet.get("dirt").set(0x25, Global.tileSet.get("dirt").get(0x0B));
        Global.tileSet.get("dirt").set(0x27, Global.tileSet.get("dirt").get(0x0B));
        Global.tileSet.get("dirt").set(0x2D, Global.tileSet.get("dirt").get(0x0B));
        Global.tileSet.get("dirt").set(0x2B, Global.tileSet.get("dirt").get(0x0B));
        Global.tileSet.get("dirt").set(0x2F, Global.tileSet.get("dirt").get(0x0B));

        Global.tileSet.get("dirt").set(0x12, Global.tileSet.get("dirt").get(0x16));
        Global.tileSet.get("dirt").set(0x17, Global.tileSet.get("dirt").get(0x16));
        Global.tileSet.get("dirt").set(0x85, Global.tileSet.get("dirt").get(0x16));
        Global.tileSet.get("dirt").set(0x87, Global.tileSet.get("dirt").get(0x16));
        Global.tileSet.get("dirt").set(0x95, Global.tileSet.get("dirt").get(0x16));
        Global.tileSet.get("dirt").set(0x96, Global.tileSet.get("dirt").get(0x16));
        Global.tileSet.get("dirt").set(0x97, Global.tileSet.get("dirt").get(0x16));

        Global.tileSet.get("dirt").set(0x48, Global.tileSet.get("dirt").get(0x68));
        Global.tileSet.get("dirt").set(0x69, Global.tileSet.get("dirt").get(0x68));
        Global.tileSet.get("dirt").set(0xA1, Global.tileSet.get("dirt").get(0x68));
        Global.tileSet.get("dirt").set(0xA9, Global.tileSet.get("dirt").get(0x68));
        Global.tileSet.get("dirt").set(0xE1, Global.tileSet.get("dirt").get(0x68));
        Global.tileSet.get("dirt").set(0xE8, Global.tileSet.get("dirt").get(0x68));
        Global.tileSet.get("dirt").set(0xE9, Global.tileSet.get("dirt").get(0x68));

        Global.tileSet.get("dirt").set(0x50, Global.tileSet.get("dirt").get(0xD0));
        Global.tileSet.get("dirt").set(0xA4, Global.tileSet.get("dirt").get(0xD0));
        Global.tileSet.get("dirt").set(0xB4, Global.tileSet.get("dirt").get(0xD0));
        Global.tileSet.get("dirt").set(0xD4, Global.tileSet.get("dirt").get(0xD0));
        Global.tileSet.get("dirt").set(0xE4, Global.tileSet.get("dirt").get(0xD0));
        Global.tileSet.get("dirt").set(0xF0, Global.tileSet.get("dirt").get(0xD0));
        Global.tileSet.get("dirt").set(0xF4, Global.tileSet.get("dirt").get(0xD0));

        MakeHammingSet(0x5A, hamming);
        for(int Value : hamming){
            if(Global.terrainTiles.get("dirt-ab").equals(Global.tileSet.get("dirt").get(Value | 0x24))){
                //System.out.println("Setting Alias From Hamming");
                Global.tileSet.get("dirt").set((Value | 0x24), Global.tileSet.get("dirt").get(0x7E));
            }
            if(Global.terrainTiles.get("dirt-ab").equals(Global.tileSet.get("dirt").get(Value | 0x81))){
                //System.out.println("Setting Alias From Hamming");
                Global.tileSet.get("dirt").set((Value | 0x81), Global.tileSet.get("dirt").get(0xDB));
            }
        }

        Global.tileSet.get("dirt").set(0x1D, Global.tileSet.get("dirt").get(0xFF));
        Global.tileSet.get("dirt").set(0x3D, Global.tileSet.get("dirt").get(0xFF));
        Global.tileSet.get("dirt").set(0x9D, Global.tileSet.get("dirt").get(0xFF));

        Global.tileSet.get("dirt").set(0x63, Global.tileSet.get("dirt").get(0xFF));
        Global.tileSet.get("dirt").set(0x67, Global.tileSet.get("dirt").get(0xFF));
        Global.tileSet.get("dirt").set(0xE3, Global.tileSet.get("dirt").get(0xFF));

        Global.tileSet.get("dirt").set(0xB8, Global.tileSet.get("dirt").get(0xFF));
        Global.tileSet.get("dirt").set(0xB9, Global.tileSet.get("dirt").get(0xFF));
        Global.tileSet.get("dirt").set(0xBC, Global.tileSet.get("dirt").get(0xFF));

        Global.tileSet.get("dirt").set(0xC6, Global.tileSet.get("dirt").get(0xFF));
        Global.tileSet.get("dirt").set(0xC7, Global.tileSet.get("dirt").get(0xFF));
        Global.tileSet.get("dirt").set(0xE6, Global.tileSet.get("dirt").get(0xFF));

        Global.tileSet.get("dirt").set(0xBD, Global.tileSet.get("dirt").get(0xFF));
        Global.tileSet.get("dirt").set(0xE7, Global.tileSet.get("dirt").get(0xFF));
    }


    private int getCorrectTile(String tileType, int row, int col){

        if(row == 0 || row == 65 || col == 0 || col == 97){
            //This is for extra tiles around edge of map
            // return 2 because rock-2 and dirt-2 are real tiles
            return 2;
        }

        int index = 0;
        int pos = 1;
        int counter = 0;

        for(int i = -1; i < 2; i++){

            for(int j = -1; j < 2; j++){

                //Skip the middle (or current tile)
                if(i == 0 && j == 0){
                    continue;
                }

                //Already have mask of 1 on the first index, so don't want to increase on first tile
                if( i == -1 && j == -1){

                }
                else{
                    pos = pos * 2;
                }

                switch (tileType.toCharArray()[0]){
                    case 'G':
                        //Space is for water
                        if(Character.toString(Global.storedMapArray[row + i][col + j]).equals("R") || Character.toString(Global.storedMapArray[row + i][col + j]).equals("D") || Character.toString(Global.storedMapArray[row + i][col + j]).equals(" ")){
                            counter = 1;
                            index = index + pos;
                        }
                        break;
                    default:
                        if(Character.toString(Global.storedMapArray[row + i][col + j]).equals(tileType)){
                            index = index + pos;

                        }
                }
            }
        }

        //Counter is set so we know to render dirt instead of grass since if we return 255, we will render grass-0
        if(counter != 1 && tileType.equals("G")){
            return 255;
        }
        return index ;
    }

    public static int getCorrectTreeTile(String tileType, int row, int col){
        int index = 0;
        int pos = 1;

        for(int i = 0; i < 2; i++){
            for(int j = -1; j < 2; j++){
                if( i == 0 && j == -1){
                }
                else{
                    pos = pos * 2;
                }

                if(Character.toString(Global.storedMapArray[row + i][col + j]).equals(tileType)){
                    index = index + pos;
                }
                else{
                    //nop (does this even have to be here?)
                }
            }
        }

        return index ;
    }


    public static void CheckIfTreeTop(int row, int col){
        if(row==0 || row==65 || col==0 || col==65){
            return;
        }
        if(Global.storedMapArray[row + 1][col] == 'F'){
            TiledMapTileLayer.Cell cell = new TiledMapTileLayer.Cell();
            cell.setTile(new StaticTiledMapTile(Global.tileSet.get("tree").get(getCorrectTreeTile("F", row, col))));
            treeTopLayer.setCell(col, row, cell);
        }
    }


    void MakeHammingSet(int value, Vector<Integer> hammingSet){
        int BitCount, Anchor, LastEnd;
        hammingSet.clear();
        for(int Index = 0; Index < 8; Index++){
            int Value = 1<<Index;
            if((value & Value) != 0){
                hammingSet.add(Value);
            }// if any of the set bits in value match any set bits in Value
        }
        LastEnd = BitCount = hammingSet.size();
        Anchor = 0;
        for(int TotalBits = 1; TotalBits < BitCount; TotalBits++){
            for(int LastIndex = Anchor; LastIndex < LastEnd; LastIndex++){
                for(int BitIndex = 0; BitIndex < BitCount; BitIndex++){
                    int NewValue = hammingSet.get(LastIndex) | hammingSet.get(BitIndex);
                    if(NewValue != hammingSet.get(LastIndex)){
                        boolean Found = false;
                        for(int Index = LastEnd; Index < hammingSet.size(); Index++){
                            if(NewValue == hammingSet.get(Index)){
                                Found = true;
                                break;
                            }
                        }
                        if(!Found){
                            hammingSet.add(NewValue);
                        }
                    }
                }
            }
            Anchor = LastEnd + 1;
            LastEnd = hammingSet.size();
        }
    }
}
