package helperclasses;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import AssetActor.PlayerAsset;

import static com.mygdx.game.Global.selectedAssets;



/**
 * Created by DrMario on 10/24/2015.
 * Contains the logic of the interactions between the main map and the UI
 */

public class UIHelper {
    //TODO: Move the initialization variables out of here and place it into Play seeing how it's only used once
    public int UIXposition, UIYPosition, UIStageWidth,UIStageHeight;
    public int MapXposition, MapYPosition, MapStageWidth,MapStageHeight;
    public int MinimapX, MinimapY,MinimapWidth,MinimapHeight;
    public int ResourceWidth,ResourceHeight,GoldX,LumberX,FoodX,ResourceY;
    public int GoldFontX,LumberFontX,FoodFontX;
    public int Width,Height;

    private static String assetName;
    public static int assetHitPoints,assetArmor,assetSpeed,assetSight,assetRange;

    public Stage uiStage;
    private static Table commandListTable;
    private static Table infoTable;
    private Table nestedTable;
    private static Table nestedTable2;
    private static HashMap<String, TextureRegion> iconTiles;
    private TextureRegion tile;
    private static Image assetImage,tempImage;
    //private BitmapFont bFont;
    private Label.LabelStyle labelStyle;

    private static HashMap<String, ClickListener> listenerList;
    private static Label selectedUnitName, hitPoints, unitArmor,unitDamage,unitRange,unitSight,unitSpeed;

    public UIHelper(){

        /*move this shit out of here from here....*/
        // ^^ Watch yo profanity
        Width = Gdx.graphics.getWidth();
        Height = Gdx.graphics.getHeight();
        UIXposition = 0;
        UIYPosition = 0;
        UIStageWidth = Width * 1 / 4;
        UIStageHeight = Height;
        MinimapX = UIStageWidth * 1/100;
        MinimapY = UIStageHeight - UIStageHeight * 10/28;
        MinimapWidth = UIStageWidth * 8/10;
        MinimapHeight = UIStageHeight * 17/50;
        MapXposition = UIStageWidth;
        MapYPosition = 0;
        MapStageWidth = Gdx.graphics.getWidth() * 3/4;
        MapStageHeight = (int)(Gdx.graphics.getHeight() * 9.5/10);
        ResourceWidth = UIStageWidth * 1/10;
        ResourceHeight = UIStageWidth * 1/10;
        GoldX = UIStageWidth * 1/6;
        LumberX = UIStageWidth * 3/6;
        FoodX = UIStageWidth * 5/6;
        ResourceY = UIStageHeight * 1/22;
        GoldFontX = UIStageWidth * 3/12;
        LumberFontX =UIStageWidth * 7/12;
        FoodFontX = UIStageWidth * 11/12;
        assetName = "";
        assetHitPoints = 0;
        assetArmor = 0;
        assetSpeed = 0;
        assetSight = 0;
        assetRange = 0;

        nestedTable = new Table();
        nestedTable2 = new Table();

        CreateListenerList();

        labelStyle = new Label.LabelStyle();
        labelStyle.font= new BitmapFont();

        uiStage = new Stage();
        uiStage.getViewport().setScreenSize(UIStageWidth, UIStageHeight);
        //uiStage.setDebugAll(true);
        iconTiles= new HashMap<String, TextureRegion>(); //holds icons

        commandListTable = new Table(); //grid for UI command list;
        commandListTable.setHeight(UIStageWidth * 1 / 2);
        commandListTable.setWidth(UIStageWidth * 3.5f);
        SplitAndLoadIcon("Icons.dat", "Icons.png");
        commandListTable.setPosition(UIStageWidth * 2 / 9, UIStageHeight * 1 / 15);
        uiStage.addActor(commandListTable);

        infoTable = new Table();
        infoTable.setHeight(UIStageWidth / 1.2f);
        infoTable.setWidth(UIStageWidth * 3.5f);
        infoTable.setPosition(UIStageWidth * 2 / 9, UIStageHeight * 7 / 24);
        uiStage.addActor(infoTable);

        infoTable.setVisible(false);


        assetImage = new Image(iconTiles.get("town-hall"));
        assetImage.setScaling(Scaling.stretch);
        infoTable.add(assetImage).fill().width(UIStageWidth * 1f).height(UIStageWidth * 1 / 4);

        selectedUnitName = new Label(assetName,labelStyle);
        System.out.println("what is the name now2?:" + assetName);
        nestedTable = new Table();

        //Label selectedUnitName = new Label("Peasant",labelStyle);
        selectedUnitName.setFontScale(7, 2);
        selectedUnitName.setAlignment(Align.center);
        nestedTable.add(selectedUnitName).expand().fill();

        nestedTable.row();

        //Label hitPoints = new Label("HP: " + assetHitPoints,labelStyle);
        hitPoints = new Label("", labelStyle);
        hitPoints.setFontScale(7, 2);
        hitPoints.setAlignment(Align.center);
        nestedTable.add(hitPoints).expand().fill();

        uiStage.addActor(nestedTable);
        infoTable.add(nestedTable);

        infoTable.row();

        unitArmor = new Label("Armor: "+assetArmor,labelStyle);
        unitArmor.setFontScale(5, 1.5f);
        unitArmor.setAlignment(Align.center);
        nestedTable2.add(unitArmor).expand().fill();

        nestedTable2.row();
        unitDamage = new Label("Damage: 1-5",labelStyle);
        unitDamage.setFontScale(5, 1.5f);
        unitDamage.setAlignment(Align.center);
        nestedTable2.add(unitDamage).expand().fill();

        nestedTable2.row();
        unitRange = new Label("Range: "+assetRange,labelStyle);
        unitRange.setFontScale(5, 1.5f);
        unitRange.setAlignment(Align.center);
        nestedTable2.add(unitRange).expand().fill();

        nestedTable2.row();
        unitSight = new Label("Sight: "+assetSight,labelStyle);
        unitSight.setFontScale(5, 1.5f);
        unitSight.setAlignment(Align.center);
        nestedTable2.add(unitSight).expand().fill();

        nestedTable2.row();
        unitSpeed = new Label("Speed: "+assetSpeed,labelStyle);
        unitSpeed.setFontScale(5, 1.5f);
        unitSpeed.setAlignment(Align.center);
        nestedTable2.add(unitSpeed).expand().fill();

        infoTable.add(nestedTable2).expand().fill().colspan(2);
        NinePatch patch = new NinePatch(new Texture(Gdx.files.internal("border.png")),
                3, 3, 3, 3);
        NinePatchDrawable background = new NinePatchDrawable(patch);
        infoTable.setBackground(background);

    }

    private void SplitAndLoadIcon(String datFileName, String pngFileName) {
        final int PART_HEIGHT = 4066;

        int numTiles;
        String tileName;

        FileHandle handle = Gdx.files.internal(datFileName);
        InputStream inStream = handle.read();
        try {
            if (inStream != null) {
                BufferedReader buffReader = new BufferedReader(new InputStreamReader(inStream));
                buffReader.readLine(); //useless first line.
                numTiles = Integer.parseInt(buffReader.readLine()); //number of tiles in PNG
                //System.out.println("outputnumTiles="+numTiles);

                handle = Gdx.files.internal(pngFileName);
                Pixmap pixmap = new Pixmap(handle);
                int pngWidth = pixmap.getWidth(); //pngWidth is same as tileWidth
                int pngHeight = pixmap.getHeight();

                int tileHeight = pngHeight / numTiles;
                int numParts = pngHeight / PART_HEIGHT; //number of FULL (height==2048) png parts
                //System.out.println("outputpngHeight="+pngHeight);
                //System.out.println("outputtileHeight="+tileHeight);
                //System.out.println("outputnumParts="+numParts);


                int numTilesInCurrentPart;
                for (int i = 0; i < numParts + 1; i++) {//for each PNG part.+1 is for the last part
                    numTilesInCurrentPart = PART_HEIGHT / tileHeight;
                    //System.out.println("outputnumTilesCurrentPart="+numTilesInCurrentPart);

                    if (i == numParts) {
                        if (pngHeight % PART_HEIGHT == 0) {
                            break;
                        }
                        numTilesInCurrentPart = (pngHeight - (numParts * PART_HEIGHT)) / tileHeight;
                    }

                    Pixmap part = new Pixmap(pngWidth, PART_HEIGHT, Pixmap.Format.RGBA8888);
                    part.drawPixmap(pixmap, 0, 0, 0, i * PART_HEIGHT, pngWidth, PART_HEIGHT); //Draw region of whole terrain
                    Texture texture = new Texture(part);

                    for (int j = 0; j < numTilesInCurrentPart; j++) {
                        tileName = buffReader.readLine();
                        tile = new TextureRegion(texture, 0, j * tileHeight, pngWidth, tileHeight);
                        iconTiles.put(tileName, tile);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void CreateListenerList(){
        listenerList = new HashMap<String, ClickListener>();
        listenerList.put("human-weapon-1", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("HUMAN-WEAPON-1", "Human weapon 1");
            }
        });
        listenerList.put("human-move", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("HUMAN-MOVE", "Human move");
            }
        });
        listenerList.put("polymorph", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("POLYMORPH", "Polymorph");
            }
        });
        listenerList.put("mine", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("MINE", "Mine");
            }
        });
        listenerList.put("repair", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("REPAIR", "Repair");
            }
        });
        listenerList.put("build-simple", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("BUILD-SIMPLE", "Build simple");
            }
        });
        listenerList.put("human-armor-1", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("HUMAN-ARMOR-1", "Human armor 1");
            }
        });
        listenerList.put("human-patrol", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("HUMAN-PATROL", "Human patrol");
            }
        });
        listenerList.put("peasant", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("PEASANT", "Peasant");
            }
        });
        listenerList.put("keep", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("KEEP", "Keep");
            }
        });
        listenerList.put("footman", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("FOOTMAN", "footman");
            }
        });
        listenerList.put("archer", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("A", "a");
            }
        });
        listenerList.put("human-weapon-2", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("w2", "w2");
            }
        });
        listenerList.put("human-armor-2", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("a2", "a2");
            }
        });
        listenerList.put("human-arrow-2", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ar2", "ar2");
            }
        });
        listenerList.put("ranger", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("R", "r");
            }
        });
        listenerList.put("human-guard-tower", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("GT", "gt");
            }
        });
        listenerList.put("human-cannon-tower", new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("CT", "ct");
            }
        });
    }

    private void ChangeRsrcDisplay(int deltaLumber, int deltaGold, int deltaFood){

    } //called whenever resource amounts change (should not affect player amounts but should reflect it)

    public static void ChangeInfo(String icon,String asset, int hitpoints, int armor, int speed, int sight, int range,List<String> capabilityList){

        if(infoTable.getCell(assetImage)!=null) {
            tempImage = new Image(iconTiles.get(icon));
            infoTable.getCell(assetImage).setActor(tempImage);
            System.out.print("gettempcell=" + infoTable.getCell(tempImage));
        }
        else{
            assetImage = new Image(iconTiles.get(icon));
            infoTable.getCell(tempImage).setActor(assetImage);
            System.out.print("getassetcell=" + infoTable.getCell(assetImage));
        }


        //System.out.print("getcell=" + infoTable.getCell(tempImage));
        selectedUnitName.setText(asset);
        //System.out.println("what is the name now?:" + selectedUnitName.getText() + "asset=" + asset);
        //assetHitPoints = hitpoints;
        //selectedUnitName.setText("");

        hitPoints.setText("HP: " + Integer.toString(hitpoints));
        unitArmor.setText("Armor: "+Integer.toString(armor));
        unitSpeed.setText("Speed: "+Integer.toString(speed));
        unitSight.setText("Sight: "+Integer.toString(sight));
        unitRange.setText("Range: "+Integer.toString(range));

        commandListTable.clearChildren();
        NinePatch patch = new NinePatch(new Texture(Gdx.files.internal("border.png")),
                3, 3, 3, 3);
        NinePatchDrawable background = new NinePatchDrawable(patch);
        commandListTable.setBackground(background);
        System.out.println("caplist size= "+capabilityList.size());
        if(capabilityList.size() > 3) {
            for (int i = 0; i < 3; i++) {
                Image commandButton = new Image(iconTiles.get(capabilityList.get(i)));
                commandButton.setScaling(Scaling.stretch);
                commandButton.addListener(listenerList.get(capabilityList.get(i)));
                commandListTable.add(commandButton).expand().fill().padRight(20).padTop(20).padLeft(20);
            }
            commandListTable.row();
            for(int i = 3; i < capabilityList.size();i++){
                Image commandButton = new Image(iconTiles.get(capabilityList.get(i)));
                commandButton.setScaling(Scaling.stretch);
                commandButton.addListener(listenerList.get(capabilityList.get(i)));
                commandListTable.add(commandButton).expand().fill().padRight(20).padTop(20).padLeft(20);
            }

        }
        else{
            for(int i = 0; i < capabilityList.size();i++){
                Image commandButton = new Image(iconTiles.get(capabilityList.get(i)));
                commandButton.setScaling(Scaling.stretch);
                commandButton.addListener(listenerList.get(capabilityList.get(i)));
                commandListTable.add(commandButton).expand().fill().padRight(20).padTop(20).padLeft(20);
            }
            for(int i = capabilityList.size();i < 3;i++){
                Image commandButton = new Image();
                commandButton.setScaling(Scaling.stretch);
                commandListTable.add(commandButton).expand().fill().padRight(20).padBottom(20).padTop(10).padLeft(20);
            }
            commandListTable.row();
            for(int i = 3; i < 6;i++){
                Image commandButton = new Image();
                commandButton.setScaling(Scaling.stretch);
                commandListTable.add(commandButton).expand().padRight(20).padBottom(20).padTop(10).padLeft(20);
            }

        }
    }

    public static void ChangeVisibility(int i,boolean bool) {
        switch (i) {
            case 1:
                nestedTable2.setVisible(bool);
                break;
            case 2:
                infoTable.setVisible(bool);
                break;
        }
    }

}