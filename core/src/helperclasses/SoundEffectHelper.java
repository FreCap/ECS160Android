package helperclasses;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Xiao Lin Li on 10/19/2015.
 */
public class SoundEffectHelper {
    private static ArrayList<Sound> acknowledges;
    private static ArrayList<Sound> selected;
    private Random rand;
    private Sound goldMine;

    public SoundEffectHelper() {
        rand = new Random();
        if(acknowledges == null) {
            acknowledges = new ArrayList<Sound>();
        }
        if(selected == null) {
            selected = new ArrayList<Sound>();
        }

        goldMine = (Gdx.audio.newSound(Gdx.files.internal("snd/buildings/gold-mine.wav")));

        //load "acknowledges" in basic folder;
        for (int i = 0; i < 4; i++) {
            acknowledges.add(Gdx.audio.newSound(Gdx.files.internal("snd/peasant/acknowledge" + (i + 1) + ".wav")));
        }

        for (int i = 0; i < 4; i++) {
            selected.add(Gdx.audio.newSound(Gdx.files.internal("snd/peasant/selected" + (i + 1) + ".wav")));
        }



    }

    public void playPeasantAcknowledge() {
        int randomNum = rand.nextInt(4);
        acknowledges.get(randomNum).play();
    }
    public void playArcherAcknowledge() {
        int randomNum = rand.nextInt(4);
        acknowledges.get(randomNum).play();
    }
    public void playFootmanAcknowledge() {
        int randomNum = rand.nextInt(4);
        acknowledges.get(randomNum).play();
    }
    public void playRangerAcknowledge() {
        int randomNum = rand.nextInt(4);
        acknowledges.get(randomNum).play();
    }
    public void playGoldMine() {
        goldMine.play();
    }

    public void playPeasantSelected(){
        int randomNum = rand.nextInt(4);
        selected.get(randomNum).play();
    }
    public void playArcherSelected(){
        int randomNum = rand.nextInt(4);
        selected.get(randomNum).play();
    }
    public void playRangerSelected(){
        int randomNum = rand.nextInt(4);
        selected.get(randomNum).play();
    }
    public void playFootmanSelected(){
        int randomNum = rand.nextInt(4);
        selected.get(randomNum).play();
    }

    public void dispose() {
        for(Sound sound : acknowledges) {
            sound.dispose();
        }
    }
}
