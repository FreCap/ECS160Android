package helperclasses;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.mygdx.game.Global;

import java.util.ArrayList;

import AssetActor.Archer;
import AssetActor.Footman;
import AssetActor.Peasant;
import AssetActor.Ranger;

public class AssetRendererHelper {
    Animation animation;
    final String[] WALK_DIRECTION = {"walk-n", "walk-ne", "walk-e", "walk-se", "walk-s", "walk-sw", "walk-w", "walk-nw"};
    final String[] ATTACK_DIRECTION = {"attack-n","attack-ne","attack-e","attack-se","attack-s","attack-sw","attack-w","attack-nw"};
    final String[] GOLD_DIRECTION = {"gold-n","gold-ne","gold-e","gold-se","gold-s","gold-sw","gold-w","gold-nw"};
    final String[] LUMBER_DIRECTION = {"lumber-n","lumber-ne","lumber-e","lumber-se","lumber-s","lumber-sw","lumber-w","lumber-nw"};
    final String[] DEATH_DIRECTION =  {"death-n","death-ne","death-e","death-se","death-s","death-sw","death-w","death-nw"};


    public AssetRendererHelper() {
        Global.peasantAnimations = new ArrayList<ArrayList<Animation>>();
        Global.peasantStatics = new ArrayList<ArrayList<TextureRegion>>();

        Global.archerAnimations = new ArrayList<ArrayList<Animation>>();
        Global.archerStatics = new ArrayList<ArrayList<TextureRegion>>();
        Global.rangerAnimations = new ArrayList<ArrayList<Animation>>();
        Global.rangerStatics = new ArrayList<ArrayList<TextureRegion>>();
        Global.footmanAnimations = new ArrayList<ArrayList<Animation>>();
        Global.footmanStatics = new ArrayList<ArrayList<TextureRegion>>();
        Global.arrowAnimations = new ArrayList<Animation>();
        for(int i = 0; i < 5; i++){
            Global.peasantAnimations.add(new ArrayList<Animation>());
            Global.peasantStatics.add(new ArrayList<TextureRegion>());
        }
        for(int i = 0; i < 3; i++){
            Global.archerAnimations.add(new ArrayList<Animation>());
            Global.archerStatics.add(new ArrayList<TextureRegion>());
            Global.rangerAnimations.add(new ArrayList<Animation>());
            Global.rangerStatics.add(new ArrayList<TextureRegion>());
            Global.footmanAnimations.add(new ArrayList<Animation>());
            Global.footmanStatics.add(new ArrayList<TextureRegion>());
        }
        for (String dir : WALK_DIRECTION) {
            dir = "peasant" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[5];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.peasantStatics.get(Peasant.State.Walking.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.peasantAnimations.get(Peasant.State.Walking.ordinal()).add(animation);
        }
        for (String dir : WALK_DIRECTION) {
            dir = "footman" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[5];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.footmanStatics.get(Footman.State.Walking.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.footmanAnimations.get(Footman.State.Walking.ordinal()).add(animation);
        }
        for (String dir : WALK_DIRECTION) {
            dir = "archer" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[5];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.archerStatics.get(Archer.State.Walking.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.archerAnimations.get(Archer.State.Walking.ordinal()).add(animation);
        }
        for (String dir : WALK_DIRECTION) {
            dir = "ranger" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[5];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.rangerStatics.get(Ranger.State.Walking.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.rangerAnimations.get(Ranger.State.Walking.ordinal()).add(animation);
        }
        for (String dir : GOLD_DIRECTION) {
            dir = "peasant" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[5];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.peasantStatics.get(Peasant.State.CarryGold.ordinal()).add(tile);
                        Global.peasantStatics.get(Peasant.State.CarryLumber.ordinal()).add(tile);

                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.peasantAnimations.get(Peasant.State.CarryGold.ordinal()).add(animation);
            Global.peasantAnimations.get(Peasant.State.CarryLumber.ordinal()).add(animation);
        }
        for (String dir : LUMBER_DIRECTION) {
            dir = "peasant" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[5];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.peasantStatics.get(Peasant.State.CarryLumber.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.peasantAnimations.get(Peasant.State.CarryLumber.ordinal()).add(animation);
        }

        for (String dir : ATTACK_DIRECTION) {
            dir = "peasant" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[5];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.peasantStatics.get(Peasant.State.Attacking.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.peasantAnimations.get(Peasant.State.Attacking.ordinal()).add(animation);
        }
       /* for (String dir : ATTACK_DIRECTION) {
            dir = "arrow" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[2];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.1f, walkFrames);
            Global.arrowAnimations.get(0).add(animation);
        } */
        for (String dir : ATTACK_DIRECTION) {
            dir = "footman" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[5];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.footmanStatics.get(Footman.State.Attacking.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.footmanAnimations.get(Footman.State.Attacking.ordinal()).add(animation);
        }
        for (String dir : ATTACK_DIRECTION) {
            dir = "archer" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[2];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.archerStatics.get(Archer.State.Attacking.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.archerAnimations.get(Archer.State.Attacking.ordinal()).add(animation);
        }
        for (String dir : ATTACK_DIRECTION) {
            dir = "ranger" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[2];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.rangerStatics.get(Ranger.State.Attacking.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.rangerAnimations.get(Ranger.State.Attacking.ordinal()).add(animation);
        }
        for (String dir : DEATH_DIRECTION) {
            dir = "ranger" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[3];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.rangerStatics.get(Ranger.State.Dying.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.rangerAnimations.get(Ranger.State.Dying.ordinal()).add(animation);
        }
        for (String dir : DEATH_DIRECTION) {
            dir = "archer" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[3];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.archerStatics.get(Archer.State.Dying.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.archerAnimations.get(Archer.State.Dying.ordinal()).add(animation);
        }
        for (String dir : DEATH_DIRECTION) {
            dir = "footman" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[3];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.footmanStatics.get(Footman.State.Dying.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.footmanAnimations.get(Footman.State.Dying.ordinal()).add(animation);
        }
        for (String dir : DEATH_DIRECTION) {
            dir = "peasant" + dir;
            int step = 0;
            TextureRegion[] walkFrames = new TextureRegion[3];
            while (true) {
                TextureRegion tile = Global.terrainTiles.get(dir + Integer.toString(step));
                if (tile != null) {
                    walkFrames[step] = tile;
                    if(step == 0){
                        Global.peasantStatics.get(Peasant.State.Dying.ordinal()).add(tile);
                    }
                } else {
                    break;
                }
                step++;
            }
            animation = new Animation(.15f, walkFrames);
            Global.peasantAnimations.get(Peasant.State.Dying.ordinal()).add(animation);
        }
    }
}

