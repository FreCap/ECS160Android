package AssetActor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.mygdx.game.GameDataTypes.EDirection;
import com.mygdx.game.Global;
import helperclasses.TileLoaderHelper;

import static com.mygdx.game.GameDataTypes.EAssetAction.aaMineGold;
import static com.mygdx.game.GameDataTypes.EAssetAction.aaWalk;
import static com.mygdx.game.GameDataTypes.EDirection.dEast;
import static com.mygdx.game.GameDataTypes.EDirection.dNorth;
import static com.mygdx.game.GameDataTypes.EDirection.dNorthEast;
import static com.mygdx.game.GameDataTypes.EDirection.dNorthWest;
import static com.mygdx.game.GameDataTypes.EDirection.dSouth;
import static com.mygdx.game.GameDataTypes.EDirection.dSouthEast;
import static com.mygdx.game.GameDataTypes.EDirection.dSouthWest;
import static com.mygdx.game.GameDataTypes.EDirection.dWest;

public class Peasant extends PlayerAsset {

    public enum State {
        Walking,
        CarryGold,
        CarryLumber,
        Attacking,
        Dying,
        Building,
        Mining,
        Storing,
    }

    private int step, Dstep;
    private int targetX, targetY;
    private boolean moving;
    int offsetX, offsetY;
    private State state;
    private boolean isMining;
    private boolean isWalkingBack;
    private float stateTime, drawstateTime;
    private int oldTargetX, oldTargetY;
    private TextureRegion image;

    public Boolean getMoving() {
        return moving;
    }

    public void setMoving(Boolean m) {
        this.moving = m;
    }

    public Peasant(int tileX, int tileY) {
        super("Peasant", tileX, tileY);

        stateTime = 0;
        drawstateTime = 0;
        isMining = false;
        isWalkingBack = false;
        targetX = tileX;
        targetY = tileY;
        direction = dSouth;

        Dstep = 0;
        offsetY = 0;
        offsetX = 0;
        state = State.Walking; //0 indicates standing, 1 is walking (should enum later) there should also be enums for other op's cutting mining etc
        moving = false;
        image = Global.peasantStatics.get(State.Walking.ordinal()).get(dSouth.ordinal());
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getTargetX() {
        return targetX;
    }

    public void setTargetX(int x) {
        this.targetX = x;
    }

    public int getTargetY() {
        return targetY;
    }

    public void setTargetY(int tileY) {
        this.targetY = tileY;
    }

    public int getStep() {
        return step;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }


    public void setOffsetX(int x) {
        this.offsetX = x;
    }

    public void setOffsetY(int tileY) {
        this.offsetY = tileY;
    }

    public boolean getIsMing() {
        return isMining;
    }

    public void incrementStep() {
        if (step > 3) {
            step = 0;
        } else step++;
    }

    public int getDStep() {
        return Dstep;
    }

    public void incrementDStep() {
        if (Dstep > 5) {
            Dstep = 0;
        } else Dstep++;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void resetStep() {
        step = 0;
    }

    public void setTilePosition(int tileX, int tileY) {
        setPosition(tileX * 32, tileY * 32);
        this.tileX = tileX;
        this.tileY = tileY;
    }

    public EDirection getDirection() {
        return direction;
    }

    public void setDirection(EDirection direction) {
        this.direction = direction;
    }

    private void replaceRock(int x, int y){
        TiledMapTileLayer layer = (TiledMapTileLayer) Global.map.getLayers().get(0);
        TiledMapTileLayer.Cell cell = layer.getCell(x, y);
        cell.setTile(new StaticTiledMapTile(Global.terrainTiles.get("rock-0")));
        Global.storedMapArray[y][x] = 'D';

    }

    private void replaceTree(int x, int y){
        TiledMapTileLayer layer = (TiledMapTileLayer) Global.map.getLayers().get(0);
        TiledMapTileLayer.Cell cell = layer.getCell(x, y);
        cell.setTile(new StaticTiledMapTile(Global.terrainTiles.get("tree-0")));
        Global.storedMapArray[y][x] = 'G';

        if(Global.storedMapArray[y][x+1] == 'F'){
            cell = layer.getCell(x+1, y);
            cell.setTile(new StaticTiledMapTile(Global.tileSet.get("tree").get(TileLoaderHelper.getCorrectTreeTile("F", y, x+1))));
        }//rerender right
        if(Global.storedMapArray[y][x-1] == 'F'){
            cell = layer.getCell(x-1, y);
            cell.setTile(new StaticTiledMapTile(Global.tileSet.get("tree").get(TileLoaderHelper.getCorrectTreeTile("F", y, x - 1))));
        }//rerender left

        if(Global.storedMapArray[y-1][x] == 'F'){
            cell = layer.getCell(x, y-1);
            cell.setTile(new StaticTiledMapTile(Global.tileSet.get("tree").get(TileLoaderHelper.getCorrectTreeTile("F", y-1, x))));
        }//rerender above
        else{
            TileLoaderHelper.treeTopLayer.getCell(x, y-1).setTile(null);
            TileLoaderHelper.CheckIfTreeTop(y - 1, x - 1);
            TileLoaderHelper.CheckIfTreeTop(y - 1, x + 1);
        }//else the above isn't a forest so we have to render a tree

        if(Global.storedMapArray[y+1][x-1] == 'F'){
            cell = layer.getCell(x-1, y+1);
            cell.setTile(new StaticTiledMapTile(Global.tileSet.get("tree").get(TileLoaderHelper.getCorrectTreeTile("F", y+1, x-1))));
        }//rerender bottom left
        if(Global.storedMapArray[y+1][x+1] == 'F'){
            cell = layer.getCell(x+1, y+1);
            cell.setTile(new StaticTiledMapTile(Global.tileSet.get("tree").get(TileLoaderHelper.getCorrectTreeTile("F", y + 1, x + 1))));
        }//rerender bottom right
        if(Global.storedMapArray[y-1][x-1] == 'F'){
            cell = layer.getCell(x-1, y-1);
            cell.setTile(new StaticTiledMapTile(Global.tileSet.get("tree").get(TileLoaderHelper.getCorrectTreeTile("F", y-1, x-1))));
        }//rerender bottom left
        if(Global.storedMapArray[y-1][x+1] == 'F'){
            cell = layer.getCell(x+1, y-1);
            cell.setTile(new StaticTiledMapTile(Global.tileSet.get("tree").get(TileLoaderHelper.getCorrectTreeTile("F", y-1, x+1))));
        }//rerender bottom right

        if(Global.storedMapArray[y+1][x] == 'F'){
            TileLoaderHelper.CheckIfTreeTop(y, x);
        }

    }


    public void moveCommand(int tileX, int tileY) {
        //for now we will stick with moving in 1 direction
        int deltaX = tileX - this.tileX;
        //check to make sure deltaX isnt 0
        int deltaY = tileY - this.tileY;
        setTargetX(tileX);
        setTargetY(tileY);
        //this.setPosition(x,tileY);
        this.setMoving(true);
        //this.incrementStep();
        //this.setDirection();
        //Gdx.app.log("deltas", "deltaX: " + deltaX + " deltaY: " + deltaY);
        Global.soundEffectHelper.playPeasantAcknowledge();

        if (deltaX == 0) {
            if (deltaY < 0) {
                this.setDirection(dNorth);
            }
            else {
                this.setDirection(dSouth);
            }
            return;
        }
        int slope = deltaY / deltaX;
        if (deltaX > 0) {
            if (slope >= 2) {
                this.setDirection(dSouth);
            }
            else if (slope >= .5) {
                this.setDirection(dSouthEast);
            }
            else if (slope >= -.5) {
                this.setDirection(dEast);
            }
            else if (slope >= -2) {
                this.setDirection(dNorthEast);
            }
            else {
                this.setDirection(dNorth);
            }

        } else {
            if (slope >= 2) {
                this.setDirection(dNorth);
            }
            else if (slope >= .5) {
                this.setDirection(dNorthWest);
            }
            else if (slope >= -.5) {
                this.setDirection(dWest);
            }
            else if (slope >= -2) {
                this.setDirection(dSouthWest);
            }
            else {
                this.setDirection(dSouth);
            }
        }
    }


    private void update() {
        Command command = getCommand();
        if (command == null) {
            moving = false;
            return;
        }
        switch (command.action) {
            case aaWalk:
                if (command.targetTileX == tileX && command.targetTileY == tileY) {
                    popCommandList();
                    for (TownHall townhall : Global.allTownHalls) {
                        if (overlap(townhall)) {
                            //TODO: set a delay to stop inside the time hall.
                            if (state == State.CarryLumber) {
                                Global.playerData.changeLumberAmount(lumber);
                                lumber = 0;
                                state = State.Walking;
                                addCommand(new Command(aaWalk, oldTargetX, oldTargetY));
                                //addCommand(new Command(aaHarvestLumber, oldTargetX, oldTargetY));
                            }
                            else if (state == State.CarryGold) {
                                Global.playerData.changeGoldAmount(gold);
                                gold = 0;
                                state = State.Walking;
                                addCommand(new Command(aaWalk, oldTargetX, oldTargetY));
                                addCommand(new Command(aaMineGold, oldTargetX, oldTargetY));
                            }
                            else if (state == State.CarryLumber){
                                Global.playerData.changeStonesAmount(stone);
                                stone = 0;
                                state = State.Walking;
                                addCommand(new Command(aaWalk, oldTargetX, oldTargetY));
                                //addCommand(new Command(aaQuarryStone, oldTargetX, oldTargetY));
                            }
                        }
                    }
                }
                else {
                    if (!moving) {
                        moveCommand(command.targetTileX, command.targetTileY);
                        moving = true;
                    }
                }
                break;
            case aaHarvestLumber: //harvesting lumber, not really attacking.
                Global.selectedAssets.remove(this); //to remove the border after harvesting/mining vv
                moving = false;
                state = State.Attacking;           //for rendering attack animation.
                stateTime += Gdx.graphics.getDeltaTime();
                if (stateTime >= 2) {
                    replaceTree(command.targetTileX, command.targetTileY);
                    MakeNewEdgeActor(command.targetTileX, command.targetTileY, 'F', Global.mapGroup, command);
                    Global.selectedTileActor.remove(); // removes actor from the group
                    Global.selectedTileActor = null;

                    lumber = 100;
                    stateTime = 0;
                    popCommandList();
                    addCommand(new Command(aaWalk, 15, 15));
                    oldTargetX = command.targetTileX;
                    oldTargetY = command.targetTileY;
                    state = State.CarryLumber;
                    return;
                }
                break;
            case aaQuarryStone:
                Global.selectedAssets.remove(this);
                moving = false;
                state = State.Attacking;
                stateTime += Gdx.graphics.getDeltaTime();
                if (stateTime >= 2) {
                    replaceRock(command.targetTileX, command.targetTileY);
                    MakeNewEdgeActor(command.targetTileX, command.targetTileY, 'R', Global.mapGroup, command);
                    Global.selectedTileActor.remove(); // removes actor from the group
                    Global.selectedTileActor = null;

                    stone = 100;
                    stateTime = 0;
                    popCommandList();
                    addCommand(new Command(aaWalk, 15, 15));
                    oldTargetX = command.targetTileX;
                    oldTargetY = command.targetTileY;
                    state = State.CarryLumber;
                    return;
                }
                break;
            case aaMineGold:
                Global.selectedAssets.remove(this);
                moving = false;
                stateTime += Gdx.graphics.getDeltaTime();
                state = State.Mining;
                for (GoldMine goldMine : Global.selectedGoldMine) {
                    if (overlap(goldMine)) {
                        goldMine.addMiningPeasant(this);
                    }
                }

                if (stateTime >= 2) {
                    for (GoldMine goldMine : Global.selectedGoldMine) {
                        if (overlap(goldMine)) {
                            goldMine.removeMiningPeasant(this);
                        }
                    }
                    gold = 100;
                    stateTime = 0;
                    popCommandList();
                    addCommand(new Command(aaWalk, 9, 9));
                    oldTargetX = command.targetTileX;
                    oldTargetY = command.targetTileY;
                    state = State.CarryGold;
                    return;
                }
            default:
        }
    }

    private void MakeNewEdgeActor(int mapX, int mapY, char rsrcType, Group group, Command command){
        if(Global.storedMapArray[mapY][mapX-1] == rsrcType && group.hit((command.targetTileX - 1) * 32, command.targetTileY * 32, true) == null){
            group.addActor(new TileActor(null, Character.toString(rsrcType), command.targetTileY, command.targetTileX - 1, false));
        } //check left of the harvested resource
        if(Global.storedMapArray[mapY][mapX+1] == rsrcType && group.hit((command.targetTileX + 1) * 32, command.targetTileY * 32, true) == null){
            group.addActor(new TileActor(null, Character.toString(rsrcType), command.targetTileY, command.targetTileX + 1, false));
        } //check right of the harvested resource
        if(Global.storedMapArray[mapY-1][mapX] == rsrcType && group.hit(command.targetTileX * 32, (command.targetTileY - 1) * 32, true) == null){
            group.addActor(new TileActor(null, Character.toString(rsrcType), command.targetTileY - 1, command.targetTileX, false));
        } //check below the harvested resource
        if(Global.storedMapArray[mapY+1][mapX] == rsrcType && group.hit(command.targetTileX * 32, (command.targetTileY + 1) *32, true) == null){
            group.addActor(new TileActor(null, Character.toString(rsrcType), command.targetTileY + 1, command.targetTileX, false));
        } //check above the harvested resource
        if(Global.storedMapArray[mapY-1][mapX-1] == rsrcType && group.hit((command.targetTileX - 1) * 32, (command.targetTileY - 1) *32, true) == null){
            group.addActor(new TileActor(null, Character.toString(rsrcType), command.targetTileY - 1, command.targetTileX - 1, false));
        } //check up-left of the harvested resource
        if(Global.storedMapArray[mapY-1][mapX+1] == rsrcType && group.hit((command.targetTileX + 1) * 32, (command.targetTileY - 1) *32, true) == null){
            group.addActor(new TileActor(null, Character.toString(rsrcType), command.targetTileY - 1, command.targetTileX + 1, false));
        } //check up-right of the harvested resource
        if(Global.storedMapArray[mapY+1][mapX-1] == rsrcType && group.hit((command.targetTileX - 1) * 32, (command.targetTileY + 1) *32, true) == null){
            group.addActor(new TileActor(null, Character.toString(rsrcType), command.targetTileY + 1, command.targetTileX - 1, false));
        } //check up-left of the harvested resource
        if(Global.storedMapArray[mapY+1][mapX+1] == rsrcType && group.hit((command.targetTileX + 1) * 32, (command.targetTileY + 1) *32, true) == null){
            group.addActor(new TileActor(null, Character.toString(rsrcType), command.targetTileY + 1, command.targetTileX + 1, false));
        } //check up-right of the harvested resource
    }//called when a rock or forest is harvested and a new edge actor is needed in place of the harvested resource or when initializing edges

    public void draw(Batch batch, float parentAlpha) {
        update();
        drawstateTime += Gdx.graphics.getDeltaTime();
        if (state == State.Mining) { //inside goldmine
            return;
        }
        float xDraw = getX();
        float yDraw = getY();
        int offset = 6 * step;
        int offsetD = 5 * Dstep;
        image = Global.peasantAnimations.get(state.ordinal()).get(direction.ordinal()).getKeyFrame(drawstateTime, true);
        //depending on direction step will affect offset
        if (State.Attacking == state) {
            batch.draw(image, tileX * 32 - 16, tileY * 32 - 16);
            return;
        }

        if (targetX == tileX && tileY == targetY) {
            moving = false;
        }
        else if (targetX == tileX) { //at target x not target y yet
            if (tileY > targetY) {
                this.setDirection(dNorth);
            }
            else {
                this.setDirection(dSouth);
            }
        }
        else if (targetY == tileY) { //at target x not target y yet
            if (tileX > targetX) {
                this.setDirection(dWest);
            }
            else {
                this.setDirection(dEast);
            }
        }

        //-16 to center the peasant in grid. should be used for footmen, peasants, and archers.
        if (moving) {
            // //change I made that makes it crash
            switch (direction) {
                case dNorth:
                    batch.draw(image, (xDraw - 16), (yDraw - 16 - offset));
                    if (this.getStep() == 4) {
                        this.setTilePosition(tileX, tileY - 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(0);
                    this.setOffsetY(-1 * offset);
                    break;
                case dSouth:
                    batch.draw(image, (xDraw - 16), (yDraw - 16 + offset));
                    if (this.getStep() == 4) {
                        this.setTilePosition(tileX, tileY + 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(0);
                    this.setOffsetY(offset);
                    break;
                case dWest:
                    batch.draw(image, (xDraw - 16 - offset), (yDraw - 16));
                    if (this.getStep() == 4) {
                        this.setTilePosition(tileX - 1, tileY);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(-1 * offset);
                    this.setOffsetY(0);
                    break;
                case dEast:
                    batch.draw(image, (xDraw - 16 + offset), (yDraw - 16));
                    if (this.getStep() == 4) {
                        this.setTilePosition(tileX + 1, tileY);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(offset);
                    this.setOffsetY(0);
                    break;
                case dNorthWest:
                    batch.draw(image, (xDraw - 16 - offsetD), (yDraw - 16 - offsetD));
                    if (this.getDStep() == 6) {
                        this.setTilePosition(tileX - 1, tileY - 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(-1 * offsetD);
                    this.setOffsetY(-1 * offsetD);
                    break;
                case dSouthWest:
                    batch.draw(image, (xDraw - 16 - offsetD), (yDraw - 16 + offsetD));
                    if (this.getDStep() == 6) {
                        this.setTilePosition(tileX - 1, tileY + 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(-1 * offsetD);
                    this.setOffsetY(offsetD);
                    break;
                case dNorthEast:
                    batch.draw(image, (xDraw - 16 + offsetD), (yDraw - 16 - offsetD));
                    if (this.getDStep() == 6) {
                        this.setTilePosition(tileX + 1, tileY - 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(offsetD);
                    this.setOffsetY(-1 * offsetD);
                    break;
                case dSouthEast:
                    batch.draw(image, (xDraw - 16 + offsetD), (yDraw - 16 + offsetD));
                    if (this.getDStep() == 6) {
                        this.setTilePosition(tileX + 1, tileY + 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(offsetD);
                    this.setOffsetY(offsetD);
                    break;
            }
            // batch.draw(image, (xDraw - 16+(6*this.getStep())), (yDraw - 16));
            this.incrementStep();
            this.incrementDStep();
        }
        else {
            //isPlayingMove= false;
            batch.draw(Global.peasantStatics.get(this.getState().ordinal()).get(this.getDirection().ordinal()), getX() - 16, getY() - 16);
        }
    }
}


