package AssetActor;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Global;

/**
 * Created by YungTho on 11/8/2015.
 */
public class ScoutTower extends PlayerAsset {
    public enum BuildingState {
        CONSTRUCT0,
        CONSTRUCT1,
        INACTIVE,
        PLACE
    }

    private BuildingState state;

    public ScoutTower(final int posX, final int posY) {
        super("ScoutTower", posX, posY);
        //Global.allTownHalls.add(Farm.this);
        state = BuildingState.INACTIVE;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(Global.terrainTiles.get("scout-towerinactive"), getX(), getY());
    }
}
