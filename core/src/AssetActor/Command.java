package AssetActor;

import com.mygdx.game.GameDataTypes;

public class Command {
    public GameDataTypes.EAssetAction action;
    public int targetTileX;
    public int targetTileY;

    public Command(GameDataTypes.EAssetAction action, int targetTileX, int targetTileY) {
        this.action = action;
        this.targetTileX = targetTileX;
        this.targetTileY = targetTileY;
    }
}