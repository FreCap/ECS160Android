package AssetActor;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.game.GameDataTypes;
import com.mygdx.game.Global;


import static com.mygdx.game.GameDataTypes.EDirection.dEast;
import static com.mygdx.game.GameDataTypes.EDirection.dNorth;
import static com.mygdx.game.GameDataTypes.EDirection.dNorthEast;
import static com.mygdx.game.GameDataTypes.EDirection.dNorthWest;
import static com.mygdx.game.GameDataTypes.EDirection.dSouth;
import static com.mygdx.game.GameDataTypes.EDirection.dSouthEast;
import static com.mygdx.game.GameDataTypes.EDirection.dSouthWest;
import static com.mygdx.game.GameDataTypes.EDirection.dWest;
/**
 * Created by root on 11/9/15.
 */
public class Arrow extends Actor {
    protected int damage;
    //protected int moveRemainderX;
    //protected int moveRemainderY;
    protected int targetX;
    protected int targetY;
    protected int speed;
    protected int count;
    protected int tileX, tileY;
    public GameDataTypes.EDirection direction;
    private TextureRegion image;

    public Arrow(int tileX, int tileY, int targetX, int targetY,GameDataTypes.EDirection d ){
        this.targetX = targetX;
        this.targetY = targetY;
        this.speed = 1;
        this.count = 0;
        this.tileX = tileX;
        this.tileY = tileY;
        this.damage = 1;
        this.direction = d;
        image = Global.arrowAnimations.get(d.ordinal()).getKeyFrame(count);
    }

    public GameDataTypes.EDirection getDirection(){
        return direction;
    }
}
