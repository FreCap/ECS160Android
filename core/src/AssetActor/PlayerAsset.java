package AssetActor;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.game.GameDataTypes;
import com.mygdx.game.GameDataTypes.EAssetType;
import com.mygdx.game.GameDataTypes.EDirection;
import com.mygdx.game.Global;

import java.util.ArrayList;
import java.util.LinkedList;

import static com.mygdx.game.GameDataTypes.EDirection.dSouth;

/**
 * Created by liray on 11/7/2015.
 */
public class PlayerAsset extends Actor {
    //these variables are all in the linux version.
    protected int creationCycle;
    protected int hitPoints;
    protected int gold;
    protected int lumber;
    // stone
    protected int stone;
    protected int step;
    protected int moveRemainderX;
    protected int moveRemainderY;
    protected int tileX, tileY;
    EDirection direction;
    protected LinkedList<Command> commands;
    protected AssetType type;
    static int updateFrequency = 1;
    static int updateDivisor = 32;
    static int idCounter = 0;
    //protected ArrayList<String> capability;

    int id;

    public PlayerAsset(String typeName, int tileX, int tileY) {
        this.creationCycle = 0;
        this.type = Global.nameAssetTypeRegistry.get(typeName);
        //this.capability = new ArrayList<String>();
        //this.setCapabilities();
        this.hitPoints = type.hitPoints;
        this.gold = 0;
        this.lumber = 0;
        // stone
        this.stone = 0;

        this.step = 0;
        this.moveRemainderX = 0;
        this.moveRemainderY = 0;
        this.direction = dSouth;
        this.id = idCounter++;
        this.tileX = tileX;
        this.tileY = tileY;                    //multiply 32 because size 1 = 32 pixels.
        setBounds(tileX * 32, tileY * 32, type.size * 32, type.size * 32); //actor's position and size.
        commands = new LinkedList<Command>();
    }

    public AssetType getNonEnumType() {
        return type;
    }
    public GameDataTypes.EAssetType getType(){
        return type.type;
    }

    public int getTileX() {
        return tileX;
    }

    public int getTileY() {
        return tileY;
    }

    public void addCommand(Command command) {
        commands.add(command);
    }

    public Command getCommand() {
        return commands.peek();
    }

    public Command popCommandList() {
        return commands.poll();
    }

    public void clearCommandList() {
        commands.clear();
    }

    public int getHitPoints(){
        return hitPoints;
    }
    /*
    public void setCapabilities(){
        switch(this.getType()) {
            case atPeasant:
                capability.add("repair");
                capability.add("human-armor-1");
                capability.add("human-weapon-1");
                capability.add("build-simple");
                break;
            case atRanger:
                break;
            case atFootman:
                break;
            case atArcher:
                break;
        }
    }*/

    public boolean overlap(PlayerAsset other) {
        return getX() < other.getX() + other.getWidth() && getX() + getWidth() > other.getX()
                && getY() < other.getY() + other.getHeight() && getY() + getHeight() > other.getY();
    }



}
