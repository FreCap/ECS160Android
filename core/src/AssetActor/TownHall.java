package AssetActor;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Global;
import java.util.ArrayList;
import java.util.List;
import helperclasses.UIHelper;

/**
 * Created by Kiet Quach on 11/3/2015.
 */
public class TownHall extends PlayerAsset {

    public enum BuildingState {
        CONSTRUCT0,
        CONSTRUCT1,
        INACTIVE,
        PLACE
    }

    private BuildingState state;

    public TownHall(final int posX, final int posY) {
        super("TownHall", posX, posY);
        Global.allTownHalls.add(TownHall.this);
        state = BuildingState.INACTIVE;

    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(Global.terrainTiles.get("town-hallinactive"), getX(), getY());
    }
}
