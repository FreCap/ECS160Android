package AssetActor;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Global;

/**
 * Created by GMoney on 11/8/2015.
 */
public class LumberMill extends PlayerAsset {
    public enum BuildingState {
        CONSTRUCT0,
        CONSTRUCT1,
        INACTIVE,
        PLACE
    }

    private BuildingState state;

    public LumberMill(final int posX, final int posY) {
        super("LumberMill", posX, posY);
        //Global.allTownHalls.add(Farm.this);
        state = BuildingState.INACTIVE;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(Global.terrainTiles.get("human-lumber-millinactive"), getX(), getY());
    }
}
