package AssetActor;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Global;

/**
 * Created by DMoney on 11/8/2015.
 */
public class Barracks extends PlayerAsset {
    public enum BuildingState {
        CONSTRUCT0,
        CONSTRUCT1,
        INACTIVE,
        PLACE
    }

    private BuildingState state; // maybe useful

    public Barracks(final int posX, final int posY) {
        super("Barracks", posX, posY);
        //Global.allTownHalls.add(Farm.this); the L is this
        state = BuildingState.INACTIVE;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        // change this! need to check what to draw
        batch.draw(Global.terrainTiles.get("human-barracksinactive"), getX(), getY());
    }
}
