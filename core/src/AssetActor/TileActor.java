package AssetActor;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Global;

import static com.mygdx.game.GameDataTypes.EAssetAction.aaHarvestLumber;
import static com.mygdx.game.GameDataTypes.EAssetAction.aaQuarryStone;
import static com.mygdx.game.GameDataTypes.EAssetAction.aaWalk;

public class TileActor extends Actor {
    final protected String type;
    protected boolean canBeMovedOnTo;

    public TileActor(TextureRegion image, final String type, float x, float y, boolean canBeMovedOnTo) {
        this.type = type;
        setBounds(y * 32, x * 32, 32, 32);
        this.canBeMovedOnTo = canBeMovedOnTo;
        setDebug(true);

        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                for (PlayerAsset asset : Global.selectedAssets) {
                    switch (asset.getType()) {
                        case atPeasant:
                            asset.commands.clear();
                            ((Peasant) asset).setMoving(false);
                            asset.addCommand(new Command(aaWalk, (int) (getX() / 32), (int) (getY() / 32)));
                            Global.selectedTileActor = TileActor.this;
                            if (type.equals("F")){

                                asset.addCommand(new Command(aaHarvestLumber, (int) (getX() / 32), (int) (getY() / 32)));
                            }
                            else if (type.equals("R")){
                                asset.addCommand(new Command(aaQuarryStone, (int) (getX() / 32), (int) (getY() / 32)));
                            }
                            //uses same animation as attack for harvesting lumber/mining stones;
                            break;
                    }
                }
            }
        });
    }
}
