package AssetActor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.mygdx.game.GameDataTypes;
import com.mygdx.game.Global;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class AssetType {

    protected int color;
    protected GameDataTypes.EAssetType type;
    protected String name;
    protected int hitPoints;
    protected int armor;
    protected int sight;
    protected int constructionSight;
    protected int size;
    protected int speed;
    protected int goldCost;
    protected int lumberCost;
    protected int foodConsumption;
    protected int buildTime;
    protected int attackSteps;
    protected int reloadSteps;
    protected int basicDamage;
    protected int piercingDamage;
    protected int range;
    protected boolean[] capabilities;
    public static HashMap<String, GameDataTypes.EAssetType> nameTypeTranslation;
    private static String[] typeStrings = {
            "None",
            "Peasant",
            "Footman",
            "Archer",
            "Ranger",
            "GoldMine",
            "TownHall",
            "Keep",
            "Castle",
            "Farm",
            "Barracks",
            "LumberMill",
            "Blacksmith",
            "ScoutTower",
            "GuardTower",
            "CannonTower"};

    public static void initializeNameToTypeMap() {
        nameTypeTranslation = new HashMap<String, GameDataTypes.EAssetType>();
        nameTypeTranslation.put("None", GameDataTypes.EAssetType.atNone);
        nameTypeTranslation.put("Peasant", GameDataTypes.EAssetType.atPeasant);
        nameTypeTranslation.put("Footman", GameDataTypes.EAssetType.atFootman);
        nameTypeTranslation.put("Archer", GameDataTypes.EAssetType.atArcher);
        nameTypeTranslation.put("Ranger", GameDataTypes.EAssetType.atRanger);
        nameTypeTranslation.put("GoldMine", GameDataTypes.EAssetType.atGoldMine);
        nameTypeTranslation.put("TownHall", GameDataTypes.EAssetType.atTownHall);
        nameTypeTranslation.put("Keep", GameDataTypes.EAssetType.atKeep);
        nameTypeTranslation.put("Castle", GameDataTypes.EAssetType.atCastle);
        nameTypeTranslation.put("Farm", GameDataTypes.EAssetType.atFarm);
        nameTypeTranslation.put("Barracks", GameDataTypes.EAssetType.atBarracks);
        nameTypeTranslation.put("LumberMill", GameDataTypes.EAssetType.atLumberMill);
        nameTypeTranslation.put("Blacksmith", GameDataTypes.EAssetType.atBlacksmith);
        nameTypeTranslation.put("ScoutTower", GameDataTypes.EAssetType.atScoutTower);
        nameTypeTranslation.put("GuardTower", GameDataTypes.EAssetType.atGuardTower);
        nameTypeTranslation.put("CannonTower", GameDataTypes.EAssetType.atCannonTower);
    }

    private static GameDataTypes.EAssetType nameToType(String name) {
        GameDataTypes.EAssetType eAssetType = nameTypeTranslation.get(name);
        if (null == eAssetType) {
            return GameDataTypes.EAssetType.atNone;
        } else {
            return eAssetType;
        }
    }

    public GameDataTypes.EAssetType getType() {
        return type;
    }

    private void addCapability(GameDataTypes.EAssetCapabilityType capabilityType) {
        if (0 > capabilityType.ordinal() || capabilities.length <= capabilityType.ordinal()) {
            return;
        }
        capabilities[capabilityType.ordinal()] = true;
    }

    public static boolean loadTypes() {
    //public boolean loadTypes() {
        FileHandle handle = Gdx.files.internal("res");

        if (null == handle) {
            System.out.printf("FileIterator == nullptr\n");
            return false;
        }

        FileHandle[] handles = handle.list();
        for (FileHandle h : handles) {
            String name = h.name();
            if (name.contains(".dat")) {
                if (!load(h)) {
                    System.out.printf("Failed to load resource \"%s\".\n", name);
                } else {
                    System.out.printf("Loaded resource \"%s\".\n", name);
                }
            }
        }
        AssetType typeNone = new AssetType();
        typeNone.name = "None";
        typeNone.type = GameDataTypes.EAssetType.atNone;
        typeNone.color = 0;
        typeNone.hitPoints = 256;
        Global.nameAssetTypeRegistry.put("None", typeNone);
        return true;
    }

    private static boolean load(FileHandle handle) {
    //private boolean load(FileHandle handle) {
        String name, tempString;
        AssetType aType;
        GameDataTypes.EAssetType eAssetType;
        int capabilityCount, assetRequirementCount;
        boolean returnStatus = false;

        if (null == handle) {
            return false;
        }
        InputStream inStream = handle.read();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream));

        try {
            name = bufferedReader.readLine();
            if (null == name) {
                System.out.println("Failed to get resource type name.\n");
                return false;
            }
            eAssetType = nameToType(name);
            if ((GameDataTypes.EAssetType.atNone == eAssetType) && (name != typeStrings[GameDataTypes.EAssetType.atNone.ordinal()])) {
                System.out.printf("Unknown resource type %s.\n", name);
                return false;
            }
            aType = Global.nameAssetTypeRegistry.get(name);
            if (aType == null) {
                aType = new AssetType();
                aType.name = name;
                Global.nameAssetTypeRegistry.put(name, aType);
            }
            aType.type = eAssetType;
            aType.color = 0;
            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type name.\n");
                return returnStatus;
            }
            aType.hitPoints = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type armor.\n");
                return returnStatus;
            }
            aType.armor = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type sight.\n");
                return returnStatus;
            }
            aType.sight = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type construction sight.\n");
                return returnStatus;
            }
            aType.constructionSight = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type size.\n");
                return returnStatus;
            }
            aType.size = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type speed.\n");
                return returnStatus;
            }
            aType.speed = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type gold cost.\n");
                return returnStatus;
            }
            aType.goldCost = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type lumber cost.\n");
                return returnStatus;
            }
            aType.lumberCost = Integer.parseInt(tempString);

            // stone
            /*
            if(!bufferedReader.readLine(TempString)){
                System.out.println("Failed to get resource type stone cost.\n");
                goto LoadExit;
            }
            PlayerAssetType->DStoneCost = std::stoi(TempString);
            */

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type food consumption.\n");
                return returnStatus;
            }
            aType.foodConsumption = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type build time.\n");
                return returnStatus;
            }
            aType.buildTime = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type attack steps.\n");
                return returnStatus;
            }
            aType.attackSteps = Integer.parseInt(tempString);
            if (aType.getType() == GameDataTypes.EAssetType.atFootman) {
                System.out.println("attack steps: " + aType.attackSteps);
            }


            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type reload steps.\n");
                return returnStatus;
            }
            aType.reloadSteps = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type basic damage.\n");
                return returnStatus;
            }
            aType.basicDamage = Integer.parseInt(tempString);
            if (aType.getType() == GameDataTypes.EAssetType.atFootman) {
                System.out.println("basic damage: " + aType.basicDamage);
            }

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type piercing damage.\n");
                return returnStatus;
            }
            aType.piercingDamage = Integer.parseInt(tempString);
            if (aType.getType() == GameDataTypes.EAssetType.atFootman) {
                System.out.println("piercing damage: " + aType.piercingDamage);
            }


            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get resource type range.\n");
                return returnStatus;
            }
            aType.range = Integer.parseInt(tempString);

            tempString = bufferedReader.readLine();
            if (null == tempString) {
                System.out.println("Failed to get  count.\n");
                return returnStatus;
            }

            capabilityCount = Integer.parseInt(tempString);
            aType.capabilities = new boolean[GameDataTypes.EAssetCapabilityType.actMax.ordinal()];
            /*for (int index = 0; index < capabilityCount; index++) {
                aType.capabilities[index] = false;
            }*/
            for (int index = 0; index < capabilityCount; index++) {
                tempString = bufferedReader.readLine();
                if (tempString == null) {
                    System.out.printf("Failed to read capability %d.\n", index);
                    return returnStatus;
                }
                aType.addCapability(Capability.nameToType(tempString));
            }

            tempString = bufferedReader.readLine();
            if (tempString == null) {
                System.out.println("Failed to get asset requirement count.\n");
                return returnStatus;
            }
            assetRequirementCount = Integer.parseInt(tempString);
            for (int index = 0; index < assetRequirementCount; index++) {
                tempString = bufferedReader.readLine();
                if (null == tempString) {
                    System.out.printf("Failed to read asset requirement %d.\n", index);
                    return returnStatus;
                }
                //TODO:write java version: PlayerAssetType -> DAssetRequirements.push_back(NameToType(TempString));
            }

            returnStatus = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnStatus;
    }

    public int getArmor() {
        return armor;
    }

    public int getRange() {
        return range;
    }

    public int getSpeed() {
        return speed;
    }
    public int getSight(){
        return sight;
    }
}

