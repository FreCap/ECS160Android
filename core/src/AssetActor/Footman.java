package AssetActor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.mygdx.game.GameDataTypes;
import com.mygdx.game.Global;

import helperclasses.UIHelper;

import static com.mygdx.game.GameDataTypes.EDirection.dEast;
import static com.mygdx.game.GameDataTypes.EDirection.dNorth;
import static com.mygdx.game.GameDataTypes.EDirection.dNorthEast;
import static com.mygdx.game.GameDataTypes.EDirection.dNorthWest;
import static com.mygdx.game.GameDataTypes.EDirection.dSouth;
import static com.mygdx.game.GameDataTypes.EDirection.dSouthEast;
import static com.mygdx.game.GameDataTypes.EDirection.dSouthWest;
import static com.mygdx.game.GameDataTypes.EDirection.dWest;

/**
 * Created by root on 11/9/15.
 */
public class Footman extends PlayerAsset{

    public enum State {
        Walking,
        Attacking,
        Dying,
        Patrol,
        StandGround
    }

    private int step, Dstep;
    private int targetX, targetY;
    int offsetX, offsetY;
    private boolean moving;
    private State state;
    private float stateTime, drawstateTime;
    private int oldTargetX, oldTargetY;
    private TextureRegion image;
/*
    private class FootmanListener extends com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener {
        @Override
        public void tap(InputEvent event, float x, float y, int count, int button) {
            System.out.println(((int) x / 32 + "   " + (int) y / 32));
            Global.soundEffectHelper.playFootmanSelected();
            Global.selectedAssets.clear();
            Global.selectedAssets.add(Footman.this);
           // UIHelper.ChangeInfo("Footman", Footman.this.hitPoints, type.armor, type.speed, type.sight, type.range);
        }
    }
*/


    public Footman(int tileX, int tileY) {
        super("Footman", tileX, tileY);
        //addListener(new FootmanListener());

        stateTime = 0;
        drawstateTime = 0;
        targetX = tileX;
        targetY = tileY;
        direction = dSouth;
        Dstep = 0;
        offsetY = 0;
        offsetX = 0;
        state = State.Walking; //0 indicates standing, 1 is walking (should enum later) there should also be enums for other op's cutting mining etc
        moving = false;
        image = Global.footmanStatics.get(State.Walking.ordinal()).get(dSouth.ordinal());
    }

    public Boolean getMoving() {
        return moving;
    }

    public void setMoving(Boolean m) {
        this.moving = m;
    }


    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getTargetX() {
        return targetX;
    }

    public void setTargetX(int x) {
        this.targetX = x;
    }

    public int getTargetY() {
        return targetY;
    }

    public void setTargetY(int tileY) {
        this.targetY = tileY;
    }

    public int getStep() {
        return step;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public void setOffsetX(int x) {
        this.offsetX = x;
    }

    public void setOffsetY(int tileY) {
        this.offsetY = tileY;
    }

    public void incrementStep() {
        if (step > 3) {
            step = 0;
        } else step++;
    }

    public int getDStep() {
        return Dstep;
    }

    public void incrementDStep() {
        if (Dstep > 5) {
            Dstep = 0;
        } else Dstep++;
    }


    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void resetStep() {
        step = 0;
    }

    public void setTilePosition(int tileX, int tileY) {
        setPosition(tileX * 32, tileY * 32);
        this.tileX = tileX;
        this.tileY = tileY;
    }

    public GameDataTypes.EDirection getDirection() {
        return direction;
    }

    public void setDirection(GameDataTypes.EDirection direction) {
        this.direction = direction;
    }

    public void moveCommand(int tileX, int tileY) {
        //for now we will stick with moving in 1 direction
        int deltaX = tileX - this.tileX;
        //check to make sure deltaX isnt 0
        int deltaY = tileY - this.tileY;
        setTargetX(tileX);
        setTargetY(tileY);
        //this.setPosition(x,tileY);
        this.setMoving(true);
        //this.incrementStep();
        //this.setDirection();
        //Gdx.app.log("deltas", "deltaX: " + deltaX + " deltaY: " + deltaY);
        Global.soundEffectHelper.playFootmanAcknowledge();

        if (deltaX == 0) {
            if (deltaY < 0) {
                this.setDirection(dNorth);
            } else {
                this.setDirection(dSouth);
            }
            return;
        }
        int slope = deltaY / deltaX;
        if (deltaX > 0) {
            if (slope >= 2) {
                this.setDirection(dSouth);
            } else if (slope >= .5) {
                this.setDirection(dSouthEast);
            } else if (slope >= -.5) {
                this.setDirection(dEast);
            } else if (slope >= -2) {
                this.setDirection(dNorthEast);
            } else {
                this.setDirection(dNorth);
            }

        } else {
            if (slope >= 2) {
                this.setDirection(dNorth);
            } else if (slope >= .5) {
                this.setDirection(dNorthWest);
            } else if (slope >= -.5) {
                this.setDirection(dWest);
            } else if (slope >= -2) {
                this.setDirection(dSouthWest);
            } else {
                this.setDirection(dSouth);
            }
        }
    }


    private void update() {
        Command command = getCommand();
        if (command == null) {
            moving = false;
            return;
        }
        switch (command.action) {
            case aaWalk:
                if (command.targetTileX == tileX && command.targetTileY == tileY) {
                    popCommandList();
                }
                if (!moving) {
                    moveCommand(command.targetTileX, command.targetTileY);
                    moving = true;
                }
                break;
            case aaAttack:
                setState(State.Attacking);
                break;
            default:
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        update();
        drawstateTime += Gdx.graphics.getDeltaTime();
        float xDraw = getX();
        float yDraw = getY();
        int offset = 6 * step;
        int offsetD = 5 * Dstep;
        image = Global.footmanAnimations.get(state.ordinal()).get(direction.ordinal()).getKeyFrame(drawstateTime, true);
        //depending on direction step will affect offset
        if (State.Attacking == state) {
            batch.draw(image, tileX * 32 - 16, tileY * 32 - 16);
            return;
        }

        if (targetX == tileX && tileY == targetY) {
            moving = false;
        } else if (targetX == tileX) { //at target x not target y yet
            if (tileY > targetY) {
                this.setDirection(dNorth);
            } else {
                this.setDirection(dSouth);
            }
        } else if (targetY == tileY) { //at target x not target y yet
            if (tileX > targetX) {
                this.setDirection(dWest);
            } else {
                this.setDirection(dEast);
            }
        }

        if (moving) {
            // //change I made that makes it crash
            switch (direction) {
                case dNorth:
                    batch.draw(image, (xDraw - 16), (yDraw - 16 - offset));
                    if (this.getStep() == 4) {
                        this.setTilePosition(tileX, tileY - 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(0);
                    this.setOffsetY(-1 * offset);
                    break;
                case dSouth:
                    batch.draw(image, (xDraw - 16), (yDraw - 16 + offset));
                    if (this.getStep() == 4) {
                        this.setTilePosition(tileX, tileY + 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(0);
                    this.setOffsetY(offset);
                    break;
                case dWest:
                    batch.draw(image, (xDraw - 16 - offset), (yDraw - 16));
                    if (this.getStep() == 4) {
                        this.setTilePosition(tileX - 1, tileY);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(-1 * offset);
                    this.setOffsetY(0);
                    break;
                case dEast:
                    batch.draw(image, (xDraw - 16 + offset), (yDraw - 16));
                    if (this.getStep() == 4) {
                        this.setTilePosition(tileX + 1, tileY);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(offset);
                    this.setOffsetY(0);
                    break;
                case dNorthWest:
                    batch.draw(image, (xDraw - 16 - offsetD), (yDraw - 16 - offsetD));
                    if (this.getDStep() == 6) {
                        this.setTilePosition(tileX - 1, tileY - 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(-1 * offsetD);
                    this.setOffsetY(-1 * offsetD);
                    break;
                case dSouthWest:
                    batch.draw(image, (xDraw - 16 - offsetD), (yDraw - 16 + offsetD));
                    if (this.getDStep() == 6) {
                        this.setTilePosition(tileX - 1, tileY + 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(-1 * offsetD);
                    this.setOffsetY(offsetD);
                    break;
                case dNorthEast:
                    batch.draw(image, (xDraw - 16 + offsetD), (yDraw - 16 - offsetD));
                    if (this.getDStep() == 6) {
                        this.setTilePosition(tileX + 1, tileY - 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(offsetD);
                    this.setOffsetY(-1 * offsetD);
                    break;
                case dSouthEast:
                    batch.draw(image, (xDraw - 16 + offsetD), (yDraw - 16 + offsetD));
                    if (this.getDStep() == 6) {
                        this.setTilePosition(tileX + 1, tileY + 1);
                        this.setOffsetX(0);
                        this.setOffsetY(0);
                        break;
                    }
                    this.setOffsetX(offsetD);
                    this.setOffsetY(offsetD);
                    break;
            }
            // batch.draw(image, (xDraw - 16+(6*this.getStep())), (yDraw - 16));
            this.incrementStep();
            this.incrementDStep();
        } else {
            //isPlayingMove= false;
            batch.draw(Global.footmanStatics.get(this.getState().ordinal()).get(this.getDirection().ordinal()), getX() - 16, getY() - 16);
        }
    }
}
