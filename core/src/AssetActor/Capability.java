package AssetActor;

import com.mygdx.game.GameDataTypes;

import java.util.ArrayList;
import java.util.HashMap;
import static com.mygdx.game.GameDataTypes.EAssetCapabilityType.*;
import com.mygdx.game.Global;

public class Capability {
    private static HashMap<String,GameDataTypes.EAssetCapabilityType> nameTypeTranslation;

    public static void initializeNameTypeMap() {
        nameTypeTranslation = new HashMap<String, GameDataTypes.EAssetCapabilityType>();
        nameTypeTranslation.put("None", actNone);
        nameTypeTranslation.put("BuildPeasant", actBuildPeasant);
        nameTypeTranslation.put("BuildFootman", actBuildFootman);
        nameTypeTranslation.put("BuildArcher", actBuildArcher);
        nameTypeTranslation.put("BuildRanger", actBuildRanger);
        nameTypeTranslation.put("BuildFarm", actBuildFarm);
        nameTypeTranslation.put("BuildTownHall", actBuildTownHall);
        nameTypeTranslation.put("BuildBarracks", actBuildBarracks);
        nameTypeTranslation.put("BuildLumberMill", actBuildLumberMill);
        nameTypeTranslation.put("BuildBlacksmith", actBuildBlacksmith);
        nameTypeTranslation.put("BuildKeep", actBuildKeep);
        nameTypeTranslation.put("BuildCastle", actBuildCastle);
        nameTypeTranslation.put("BuildScoutTower", actBuildScoutTower);
        nameTypeTranslation.put("BuildGuardTower", actBuildGuardTower);
        nameTypeTranslation.put("BuildCannonTower", actBuildCannonTower);
        nameTypeTranslation.put("Move", actMove);
        nameTypeTranslation.put("Repair", actRepair);
        nameTypeTranslation.put("Mine", actMine);
        nameTypeTranslation.put("BuildSimple", actBuildSimple);
        nameTypeTranslation.put("BuildAdvanced", actBuildAdvanced);
        nameTypeTranslation.put("Convey", actConvey);
        nameTypeTranslation.put("Cancel", actCancel);
        nameTypeTranslation.put("BuildWall", actBuildWall);
        nameTypeTranslation.put("Attack", actAttack);
        nameTypeTranslation.put("StandGround", actStandGround);
        nameTypeTranslation.put("Patrol", actPatrol);
        nameTypeTranslation.put("WeaponUpgrade1", actWeaponUpgrade1);
        nameTypeTranslation.put("WeaponUpgrade2", actWeaponUpgrade2);
        nameTypeTranslation.put("WeaponUpgrade3", actWeaponUpgrade3);
        nameTypeTranslation.put("ArrowUpgrade1", actArrowUpgrade1);
        nameTypeTranslation.put("ArrowUpgrade2", actArrowUpgrade2);
        nameTypeTranslation.put("ArrowUpgrade3", actArrowUpgrade3);
        nameTypeTranslation.put("ArmorUpgrade1", actArmorUpgrade1);
        nameTypeTranslation.put("ArmorUpgrade2", actArmorUpgrade2);
        nameTypeTranslation.put("ArmorUpgrade3", actArmorUpgrade3);
        nameTypeTranslation.put("Longbow", actLongbow);
        nameTypeTranslation.put("RangerScouting", actRangerScouting);
        nameTypeTranslation.put("Marksmanship", actMarksmanship);
    }

    public static GameDataTypes.EAssetCapabilityType nameToType(String name) {
        GameDataTypes.EAssetCapabilityType type = nameTypeTranslation.get(name);
        if(null == type) {
            return actNone;
        }
        return type;
    }

    public static void initializeCapabilityList() {
        ArrayList<String> assetCapabilities = new ArrayList<String>();
        ArrayList<String> peasantCapabilities = new ArrayList<String>();
        ArrayList<String> townCapabilities = new ArrayList<String>();
        ArrayList<String> barrackCapabilities = new ArrayList<String>();
        ArrayList<String> blackCapabilties = new ArrayList<String>();
        ArrayList<String> lumberCapabilities = new ArrayList<String>();
        ArrayList<String> scoutCapabilities = new ArrayList<String>();
        ArrayList<String> farmCapabilities = new ArrayList<String>();

        //peasant
        peasantCapabilities.add("repair");
        peasantCapabilities.add("human-armor-1");
        peasantCapabilities.add("human-weapon-1");
        peasantCapabilities.add("build-simple");
        Global.CapabilitiesList.put("peasant", peasantCapabilities);

        //archer, footman, ranger have same capabilities
        assetCapabilities.add("human-armor-1");
        assetCapabilities.add("human-weapon-1");
        assetCapabilities.add("human-patrol");
        Global.CapabilitiesList.put("footman", assetCapabilities);
        Global.CapabilitiesList.put("archer", assetCapabilities);
        Global.CapabilitiesList.put("ranger", assetCapabilities);

        // townhall
        townCapabilities.add("peasant");
        townCapabilities.add("keep");
        Global.CapabilitiesList.put("townhall", townCapabilities);

        // barrack
        barrackCapabilities.add("footman");
        barrackCapabilities.add("archer");
        Global.CapabilitiesList.put("barracks", barrackCapabilities);

        // black
        blackCapabilties.add("human-weapon-2");
        blackCapabilties.add("human-armor-2");
        Global.CapabilitiesList.put("blacksmith", blackCapabilties);

        // lumber
        lumberCapabilities.add("human-arrow-2");
        lumberCapabilities.add("ranger");
        Global.CapabilitiesList.put("lumbermill", lumberCapabilities);

        // scout
        scoutCapabilities.add("human-guard-tower");
        scoutCapabilities.add("human-cannon-tower");
        Global.CapabilitiesList.put("scouttower", scoutCapabilities);

        // farm
        Global.CapabilitiesList.put("farm", farmCapabilities);
    }
}

