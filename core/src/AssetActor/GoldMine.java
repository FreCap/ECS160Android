package AssetActor;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.GameDataTypes;
import com.mygdx.game.Global;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import helperclasses.SoundEffectHelper;
import helperclasses.UIHelper;

import static com.mygdx.game.GameDataTypes.EAssetAction.*;


/**
 * Created by Kiet Quach on 11/2/2015.
 */
public class GoldMine extends PlayerAsset {
    private int totalGold;
    private ClickListener listener;
    private HashSet<Peasant> miningPeasants;
    private List<String> capabilityList;


    public GoldMine(final int posX, final int posY) {
        super("GoldMine", posX, posY);
        miningPeasants = new HashSet<Peasant>();
        capabilityList = new ArrayList<String>();
        totalGold = 10000;
        Global.allGoldMines.add(this);
        listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.out.println("Clicked goldmine");
                if (Global.selectedGoldMine.isEmpty()) {
                    Global.selectedGoldMine.add(GoldMine.this);
                }
              /*  for (AssetType asset : Global.selectedAssets) {
                    if(asset.getType() == EAssetType.atPeasant) {
                        System.out.println("Clicked on Goldmine");
                        ((Peasant)asset).moveCommand(posX, posY);
                    }
                }*/
                for (PlayerAsset asset : Global.selectedAssets) {
                    switch (asset.getType()) {
                        case atPeasant:
                            asset.commands.clear();
                            ((Peasant)asset).setMoving(false);
                            asset.addCommand(new Command(aaWalk, (int) (getX() / 32), (int) (getY() / 32)));
                            asset.addCommand(new Command(aaMineGold, (int) (getX() / 32), (int) (getY() / 32)));
                            break;
                    }
                }
                //PlayerData.setGold(100);
                UIHelper.ChangeInfo("gold-mine", "Gold Mine", GoldMine.this.hitPoints, type.armor, type.speed, type.sight, type.range, capabilityList);
                UIHelper.ChangeVisibility(2, true);
                UIHelper.ChangeVisibility(1, false);
            }

        };
        addListener(listener);

    }

    public void loseGold() {
        totalGold -= 100;
    }

    public void addMiningPeasant(Peasant peasant) {
        if(miningPeasants.add(peasant)) {
            Global.soundEffectHelper.playGoldMine();
        }
    }

    public void removeMiningPeasant(Peasant peasant) {
        miningPeasants.remove(peasant);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (miningPeasants.size() > 0) {
            batch.draw(Global.terrainTiles.get("goldactive"), getX(), getY());
        } else {
            batch.draw(Global.terrainTiles.get("goldinactive"), getX(), getY());
        }
    }
}
