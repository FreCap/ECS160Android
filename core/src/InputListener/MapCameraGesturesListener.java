package InputListener;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.mygdx.game.GameDataTypes;
import com.mygdx.game.Global;

import java.util.ArrayList;

import AssetActor.Archer;
import AssetActor.Command;
import AssetActor.Footman;
import AssetActor.Peasant;
import AssetActor.PlayerAsset;
import AssetActor.Ranger;
import helperclasses.UIHelper;

/**
 * Created by liray on 11/2/2015.
 */

public class MapCameraGesturesListener extends ActorGestureListener {
    private OrthographicCamera camera;
    Rectangle touchDetector;
    public MapCameraGesturesListener(OrthographicCamera camera) {
        super(20, 0.4f, 0.3f, 0.15f); //the 0.3f is the duration for longpress.
        this.camera = camera;
    }

    @Override
    public boolean longPress(Actor actor, float x, float y) {  //will send out move, attack, mining, or lumber commands to selected units
        for (PlayerAsset asset : Global.selectedAssets) {
            switch (asset.getType()) {
                case atPeasant:
                    asset.clearCommandList();
                    ((Peasant) asset).setMoving(false);
                    asset.addCommand(new Command(GameDataTypes.EAssetAction.aaWalk, (int) (x / 32), (int) (y / 32)));
                    break;
                case atArcher:
                    asset.clearCommandList();
                    ((Archer) asset).setMoving(false);
                    asset.addCommand(new Command(GameDataTypes.EAssetAction.aaWalk, (int) (x / 32), (int) (y / 32)));
                    break;
                case atFootman:
                    asset.clearCommandList();
                    ((Footman) asset).setMoving(false);
                    asset.addCommand(new Command(GameDataTypes.EAssetAction.aaWalk, (int) (x / 32), (int) (y / 32)));
                    break;
                case atRanger:
                    asset.clearCommandList();
                    ((Ranger) asset).setMoving(false);
                    asset.addCommand(new Command(GameDataTypes.EAssetAction.aaWalk, (int) (x / 32), (int) (y / 32)));
                    break;
            }
        }
        return true;
    }
    @Override
    public void tap(InputEvent event, float x, float y, int count, int button) { //selection, deselection only
        ArrayList<String> capability;
        int tileX = (int) (x / 32);
        int tileY = (int) (y / 32);
        System.out.println("tap occured at " + tileX + " " + tileY);
        Global.selectedAssets.clear();
        for (PlayerAsset asset : Global.playerAssets) {
            touchDetector = new Rectangle();
            touchDetector.set(asset.getTileX(), asset.getTileY(), 2, 2);
            if (((asset.getType() == GameDataTypes.EAssetType.atTownHall) && touchDetector.contains(tileX,tileY))
                    || ((asset.getType() == GameDataTypes.EAssetType.atLumberMill) && touchDetector.contains(tileX,tileY))
                    || ((asset.getType() == GameDataTypes.EAssetType.atScoutTower) && touchDetector.contains(tileX,tileY))
                    ||((asset.getType() == GameDataTypes.EAssetType.atBlacksmith) && touchDetector.contains(tileX,tileY))
                    || ((asset.getType() == GameDataTypes.EAssetType.atBarracks) && touchDetector.contains(tileX,tileY))
                    || ((asset.getType() == GameDataTypes.EAssetType.atFarm) && touchDetector.contains(tileX,tileY))
                    || (asset.getTileX() == tileX && asset.getTileY() == tileY)) { //need to add cases for all buildings (once merged with building branch (also cases for goldmine
                switch (asset.getType()) {
                    case atTownHall:
                        System.out.println("townhall is touched");
                        Global.selectedAssets.add(asset);
                        UIHelper.ChangeInfo("town-hall", "Townhall", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(), asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("townhall"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, false);
                        break;
                    case atPeasant:
                        System.out.println("peasant being selected");
                        Global.soundEffectHelper.playPeasantSelected();
                        Global.selectedAssets.add(asset);

                        UIHelper.ChangeInfo("peasant", "Peasant", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(),
                                asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("peasant"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, true);
                        break;
                    case atArcher:
                        Global.soundEffectHelper.playArcherSelected();
                        Global.selectedAssets.add(asset);
                        UIHelper.ChangeInfo("archer", "Archer", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(),
                                asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("archer"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, true);
                        break;
                    case atFootman:
                        System.out.println("footman is touched");
                        Global.soundEffectHelper.playFootmanSelected();
                        Global.selectedAssets.add(asset);
                        UIHelper.ChangeInfo("footman", "Footman", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(),
                                asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("footman"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, true);
                        break;
                    case atRanger:
                        System.out.println("Ranger is touched");
                        Global.soundEffectHelper.playRangerSelected();
                        Global.selectedAssets.add(asset);
                        UIHelper.ChangeInfo("ranger", "Ranger", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(),
                                asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("ranger"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, true);
                        break;
                    case atFarm:
                        System.out.println("farm is touched");;
                        Global.selectedAssets.add(asset);
                        UIHelper.ChangeInfo("chicken-farm", "Farm", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(),
                                asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("farm"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, false);
                        break;
                    case atBarracks:
                        System.out.println("barracks is touched");;
                        Global.selectedAssets.add(asset);
                        UIHelper.ChangeInfo("human-barracks", "Barracks", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(),
                                asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("barracks"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, false);
                        break;
                    case atLumberMill:
                        System.out.println("lumberMill is touched");;
                        Global.selectedAssets.add(asset);
                        UIHelper.ChangeInfo("human-lumber-mill", "LumberMill", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(),
                                asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("lumbermill"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, false);
                        break;
                    case atBlacksmith:
                        System.out.println("blacksmith is touched");;
                        Global.selectedAssets.add(asset);
                        UIHelper.ChangeInfo("human-blacksmith", "BlackSmith", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(),
                                asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("blacksmith"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, false);
                        break;
                    case atScoutTower:
                        System.out.println("scouttower is touched");;
                        Global.selectedAssets.add(asset);
                        UIHelper.ChangeInfo("scout-tower", "ScoutTower", asset.getHitPoints(), asset.getNonEnumType().getArmor(), asset.getNonEnumType().getSpeed(),
                                asset.getNonEnumType().getSight(), asset.getNonEnumType().getRange(), Global.CapabilitiesList.get("scouttower"));
                        UIHelper.ChangeVisibility(2, true);
                        UIHelper.ChangeVisibility(1, false);
                        break;
                }
            }
        }
    }

    @Override
    public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
        camera.translate(-deltaX * 0.70f, -deltaY * 0.70f);
        cameraConstrain();
    }

    @Override
    public void fling(InputEvent event, float velocityX, float velocityY, int button) { //moving camera
        final float velX = camera.zoom * velocityX * 0.1f;
        final float velY = camera.zoom * velocityY * 0.1f;
        final float deltaTime = Gdx.graphics.getDeltaTime(); // Retrieve the duration time of the flinging action

        Thread flingThread = new Thread() {
            @Override
            public void run() {
                try {
                    for (int i = 10; i > 0; i--) {
                        sleep(30);
                        camera.position.add(-velX * deltaTime * i, -velY * deltaTime * i, 0);
                        cameraConstrain();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        flingThread.start();
    }

    public void cameraConstrain() { //??
        camera.update();

        float camX = camera.position.x;
        float camY = camera.position.y;

        Vector2 camMin = new Vector2(camera.viewportWidth + 64, camera.viewportHeight + 64);
        Vector2 camMax = new Vector2(97 * 32, 65 * 32);
        //Weird values but this gets rid of extra tiles on edge of map


        camMin.scl(camera.zoom / 2); //bring to center and scale by the zoom level
        camMax.sub(camMin); //bring to center

        //keep camera within borders
        camX = Math.min(camMax.x, Math.max(camX, camMin.x));
        camY = Math.min(camMax.y, Math.max(camY, camMin.y));

        //set the camera positions and update
        camera.position.set(camX, camY, camera.position.z);
        camera.update();
    }

    public void zoom(InputEvent event, float initialDistance, float distance) { //needs to be replaced with dragging multiselect
        float ratio = initialDistance / distance;
        float zoom;
        zoom = MathUtils.clamp(1.0f * ratio, 0.1f, 1.0f);
        camera.zoom = zoom;
        camera.update();
    }
}
